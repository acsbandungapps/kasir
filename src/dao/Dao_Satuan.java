/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kasirMain.Koneksi;
import entity.Satuan_Barang;
import kasirMain.Constant;

/**
 *
 * @author Erdy
 */
public class Dao_Satuan {

    public static ArrayList<Satuan_Barang> list = new ArrayList<>();

    public static List<Satuan_Barang> findAllSatuanBarang() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "select * from satuan_barang where status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Satuan_Barang s = new Satuan_Barang();
                s.setId_satuan(res.getString("id_satuan"));
                s.setNama_satuan(res.getString("nama_satuan"));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Kategori.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static String generateIDSatuan() {
        String newID = "";
        try {
            String sql = "select * from satuan_barang order by id_satuan desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Satuan: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = "SAT-0000";
            } else {
                serial = rs.getString("id_satuan");
            }
            int nilai = Integer.parseInt(serial.substring(4, 8));
            System.out.println("Nilai ID Satuan: " + nilai);
            nilai = nilai + 1;
            newID = "SAT-" + String.format("%04d", nilai);
            return newID;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    public static String tambahSatuan(String namaSatuan) {
        try {
            String idSatuan = generateIDSatuan();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "insert into satuan_barang (id_satuan,nama_satuan,status) values ('" + idSatuan + "','" + namaSatuan + "','aktif')";
            System.out.println("Query SQL Insert Satuan Barang: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.INSERT_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.INSERT_FAILED_MESSAGE;
        }
    }

//    public static String hapusSatuan(Satuan_Barang satuan) {
//        try {
//            Statement statement = (Statement) Koneksi.getConnection().createStatement();
//            String sql = "DELETE FROM satuan_barang WHERE id_satuan='" + satuan.getId_satuan() + "'";
//            System.out.println(sql);
//            statement.executeUpdate(sql);
//            statement.close();
//            return "Data Sukses Dihapus";
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "Data Gagal Dihapus";
//        }
//    }
    public static String hapusSatuan(Satuan_Barang satuan) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "update satuan_barang set status='nonaktif' WHERE id_satuan='" + satuan.getId_satuan() + "' and status='aktif'";
            System.out.println("Query SQL Hapus Satuan Barang: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.DELETE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.DELETE_FAILED_MESSAGE;
        }
    }

    public static String ubahSatuan(Satuan_Barang satuan, String namaSatuan) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE satuan_barang set nama_satuan='" + namaSatuan + "' where id_satuan='" + satuan.getId_satuan() + "' and status='aktif'";
            System.out.println("Query SQL Ubah Satuan Barang: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.UPDATE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.UPDATE_FAILED_MESSAGE;
        }
    }
}
