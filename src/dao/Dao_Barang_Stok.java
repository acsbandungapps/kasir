/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.Dao_Barang_Detail.generateIDBarangDetail;
import static dao.Dao_Barang_Detail.list;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kasirMain.Constant;
import kasirMain.Koneksi;
import entity.Barang_Detail;
import entity.Barang_Stok;
import util.Function;

/**
 *
 * @author Erdy
 */
public class Dao_Barang_Stok {

    public static ArrayList<Barang_Stok> list = new ArrayList<>();

    public static List<Barang_Stok> findAllBarangStok() {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "select * from barang_stok where status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Barang_Stok bs = new Barang_Stok();
                bs.setId_stok_barang(res.getString("id_stok_barang"));
                list.add(bs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Barang_Stok.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static String generateIDBarangStok() {
        String newID = "";
        try {
            String sql = "select * from barang_stok order by id_detil_barang desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Barang Stok: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = "STK-0000";
            } else {
                serial = rs.getString("id_stok_barang");
            }
            int nilai = Integer.parseInt(serial.substring(4, 8));
            System.out.println("Nilai ID Detil Barang: " + nilai);
            nilai = nilai + 1;
            newID = "STK-" + String.format("%04d", nilai);
            return newID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    public static String tambahBarangStok(String id_barang_stok, String id_kategori_barang, String id_detil_barang, String nama_barang, int jumlah_stok, Double harga_beli, Double harga_jual, Double laba, String id_satuan) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "insert into barang_stok (id_stok_barang,id_kategori_barang,id_detil_barang,nama_barang,jumlah_stok,harga_beli,harga_jual,laba,id_satuan,status) values ('" + id_barang_stok + "','" + id_kategori_barang + "','" + id_detil_barang + "','" + nama_barang + "','" + jumlah_stok + "','" + harga_beli + "','" + harga_jual + "','" + laba + "','" + id_satuan + "','aktif')";
            System.out.println("Query SQL Insert Barang_Stok: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.INSERT_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.INSERT_FAILED_MESSAGE;
        }
    }

    public static String UbahBarangStok(String idDetilBarang, String idKategori, String namaBarang, String idSatuan, String idStokBarang, int jumlah_stok, Double harga_beli, Double harga_jual, Double laba) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE barang_stok set id_kategori_barang='" + idKategori + "',nama_barang='" + namaBarang + "',jumlah_stok='" + jumlah_stok + "',harga_beli='" + harga_beli + "',harga_jual='" + harga_jual + "',laba='" + laba + "',id_satuan='" + idSatuan + "' where id_detil_barang='" + idDetilBarang + "' and id_stok_barang='" + idStokBarang + "' and status='aktif'";
            System.out.println("Query SQL Ubah Barang Stok: "+sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.UPDATE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.UPDATE_FAILED_MESSAGE;
        }
    }

    public static String HapusBarangStok(String idDetilBarang, String idStokBarang) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE barang_stok set status='nonaktif' where id_detil_barang='" + idDetilBarang + "' and id_stok_barang='" + idStokBarang + "' and status='aktif'";
            System.out.println("Query SQL Hapus Barang Stok: "+sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.DELETE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.DELETE_FAILED_MESSAGE;
        }
    }

}
