/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.Dao_Barang_Detail.list;
import entity.Barang_Detail;
import entity.Keranjang;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kasirMain.Constant;
import kasirMain.Koneksi;
import util.Function;

/**
 *
 * @author Erdy
 */
public class Dao_Keranjang_Pembelian {

    public static List<Keranjang> list = new ArrayList<>();
    public static List<Keranjang> listIsi = new ArrayList<>();
    private static float total;
    private static int banyak;

    public static String generateIDKeranjang() {
        String newID = "";
        try {
            String sql = "select * from keranjang_penjualan order by id_keranjang desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Barang Detail: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = "KRJ-00000";
            } else {
                serial = rs.getString("id_keranjang");
            }
            int nilai = Integer.parseInt(serial.substring(4, 9));
            System.out.println("Nilai ID Keranjang: " + nilai);
            nilai = nilai + 1;
            newID = "KRJ-" + String.format("%05d", nilai);
            return newID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    // id_keranjang
    //id_kategori_barang
    //id_detil_barang
    //jumlah_beli
    //tgl_transaksi
    //waktu_transaksi
    //keterangan
    //status
    public static String tambahKeranjang(String id_kategori, String id_detil_barang, int jumlah_beli, String tgl_transaksi, String harga, String total, String waktu_transaksi, String keterangan) {
        try {
            String newHarga=Function.removeRp(Function.removeSeparator(harga));
            String id_keranjang = generateIDKeranjang();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "insert into keranjang_penjualan (id_keranjang,id_kategori,id_detil_barang,jumlah_beli,tgl_transaksi,waktu_transaksi,harga,total,keterangan,status) values('" + id_keranjang + "','" + id_kategori + "','" + id_detil_barang + "'," + jumlah_beli + ",'" + tgl_transaksi + "','" + waktu_transaksi + "','" + newHarga + "','" + total + "','" + keterangan + "','aktif')";
            System.out.println("Query SQL Insert Keranjang: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.INSERT_SUCCESS_MESSAGE;
//                
//                
//            listIsi = selectKeranjangJoinBarangDetail();
//            for (Keranjang data : list) {
//                System.out.println("data " + data.idKeranjang);
//            }
//            System.out.println("Isi List Isi: " + listIsi);
//            if (listIsi.isEmpty()) {
//                id_keranjang = generateIDKeranjang();
//                System.out.println("ID Keranjang 1: " + id_keranjang);
//                Statement statement = (Statement) Koneksi.getConnection().createStatement();
//                String sql = "insert into keranjang_penjualan (id_keranjang,id_kategori,id_detil_barang,jumlah_beli,tgl_transaksi,waktu_transaksi,keterangan,status) values('" + id_keranjang + "','" + id_kategori + "','" + id_detil_barang + "'," + jumlah_beli + ",'" + tgl_transaksi + "','" + waktu_transaksi + "','" + keterangan + "','aktif')";
//                System.out.println("Query SQL Insert Keranjang: " + sql);
//                statement.executeUpdate(sql);
//                statement.close();
//                return Constant.INSERT_SUCCESS_MESSAGE;
//            } else {
//                for (Keranjang data : list) {
//                    id_keranjang = data.getIdKeranjang();
//                    System.out.println("ID Keranjang 2: " + id_keranjang);
//                }
//                Statement statement = (Statement) Koneksi.getConnection().createStatement();
//                String sql = "insert into keranjang_penjualan (id_keranjang,id_kategori,id_detil_barang,jumlah_beli,tgl_transaksi,waktu_transaksi,keterangan,status) values('" + id_keranjang + "','" + id_kategori + "','" + id_detil_barang + "'," + jumlah_beli + ",'" + tgl_transaksi + "','" + waktu_transaksi + "','" + keterangan + "','aktif')";
//                System.out.println("Query SQL Insert Keranjang: " + sql);
//                statement.executeUpdate(sql);
//                statement.close();
//                return Constant.INSERT_SUCCESS_MESSAGE;
//            }

        } catch (Exception e) {
            e.printStackTrace();
            return Constant.INSERT_FAILED_MESSAGE;
        }
    }

    public static String selectLastIdKeranjang() {
        String id = "";
        try {

            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT keranjang_penjualan.id_keranjang FROM keranjang_penjualan WHERE keranjang_penjualan.status='aktif' limit 1";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Keranjang k = new Keranjang();
                k.setIdKeranjang(res.getString("id_keranjang"));
                id = k.getIdKeranjang();
                return id;
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return id;
    }

    public static List<Keranjang> selectKeranjangJoinBarangDetail() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM keranjang_penjualan join barang_detail on keranjang_penjualan.id_detil_barang=barang_detail.id_detil_barang \n"
                    + "join kategori_barang on kategori_barang.id_kategori=barang_detail.id_kategori JOIN satuan_barang ON satuan_barang.id_satuan=barang_detail.id_satuan \n"
                    + "WHERE keranjang_penjualan.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
//            try {
//                 ResultSet res = statement.executeQuery(sql);
//            } catch (Exception e) {
//                return null;
//            }
//            if (!res.isBeforeFirst()) {
//                list.add(null);
//                return list;
//            } else {
            while (res.next()) {
                Keranjang k = new Keranjang();
                k.setIdKeranjang(res.getString("id_keranjang"));
                k.setIdKategoriBarang(res.getString("id_kategori"));
                k.setNamaKategoriBarang(res.getString("nama_kategori"));
                k.setIdDetilBarang(res.getString("id_detil_barang"));
                k.setNamaBarang(res.getString("nama_barang"));
                k.setJumlahPembelian(res.getInt("jumlah_pembelian"));
                k.setJumlahStok(res.getInt("jumlah_stok"));
                k.setJumlah_beli(res.getInt("jumlah_beli"));
                k.setHargaBeli(res.getString("harga_beli"));
                k.setHargaJual("Rp. " + Function.returnSeparator(res.getString("harga_jual")));
                k.setTotal("Rp. " + Function.returnSeparator(res.getString("total")));
                k.setLaba(res.getString("laba"));
                k.setIdSatuan(res.getString("id_satuan"));
                k.setNamaSatuan(res.getString("nama_satuan"));
                k.setTglTransaksi(Function.returnFormattedDate(res.getString("tgl_transaksi")));
                k.setWaktuTransaksi(res.getTime("waktu_transaksi"));
                k.setKeterangan(res.getString("keterangan"));
                k.setStatus(res.getString("status"));
                list.add(k);
            }
            //}

        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);

        }
        return list;
    }

    public static float CountTotal() {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT SUM(total) as Total FROM keranjang_penjualan where status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                total = res.getFloat("total");
            }
            return total;
        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return total;
    }

    public static int CountBanyak() {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT SUM(jumlah_beli) as jumlah FROM `keranjang_penjualan` WHERE status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                banyak = res.getInt("jumlah");
            }
            return banyak;
        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return banyak;
    }

    public static void deleteKeranjang() {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE keranjang_penjualan set status='nonaktif' where status='aktif'";
            System.out.println("Query SQL Hapus Keranjang Penjualan: " + sql);
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String deleteOneItemKeranjang(String id) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE keranjang_penjualan set status='nonaktif' where id_keranjang=" + id + "";
            System.out.println("Query SQL Hapus Keranjang Penjualan: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.DELETE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.DELETE_FAILED_MESSAGE;
        }
    }

    public static String checkEmptyKeranjang() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM keranjang_penjualan join barang_detail on keranjang_penjualan.id_detil_barang=barang_detail.id_detil_barang \n"
                    + "join kategori_barang on kategori_barang.id_kategori=barang_detail.id_kategori JOIN satuan_barang ON satuan_barang.id_satuan=barang_detail.id_satuan \n"
                    + "WHERE keranjang_penjualan.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            //return Constant.FILLED_KERANJANG;
            if (!res.isBeforeFirst()) {
                return Constant.EMPTY_KERANJANG;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.EMPTY_KERANJANG;
        }
        return Constant.FILLED_KERANJANG;
    }

    //Cari ID Apa Aja yang ada di keranjang
    public static List<Keranjang> selectIdOnKeranjang(){
           try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT id_detil_barang,jumlah_beli FROM keranjang_penjualan WHERE keranjang_penjualan.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Keranjang k = new Keranjang();               
                k.setIdDetilBarang(res.getString("id_detil_barang"));
                k.setJumlah_beli(res.getInt("jumlah_beli"));
                list.add(k);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Keranjang_Pembelian.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }
    
     public static int CountJumlahBeli(String id) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT SUM(jumlah_beli) as jumlah FROM `keranjang_penjualan` WHERE id_detil_barang='"+id+"' and status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                banyak = res.getInt("jumlah");
            }
            return banyak;
        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return banyak;
    }
}
