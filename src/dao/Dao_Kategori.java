/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import kasirMain.Koneksi;
import entity.Kategori;
import kasirMain.Constant;

/**
 *
 * @author Erdy
 */
public class Dao_Kategori {

    public static ArrayList<Kategori> list = new ArrayList<>();

    public static List<Kategori> findAllKategori() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "select * from kategori_barang where status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Kategori k = new Kategori();
                k.setIdKategori(res.getString("id_kategori"));
                k.setNamaKategori(res.getString("nama_kategori"));
                list.add(k);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Kategori.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static String generateIDKategori() {
        String newID = "";
        try {
            String sql = "select * from kategori_barang order by id_kategori desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Kategori: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = "KAT-0000";
            } else {
                serial = rs.getString("id_kategori");
            }
            int nilai = Integer.parseInt(serial.substring(4, 8));
            System.out.println("Nilai ID Kategori: " + nilai);
            nilai = nilai + 1;
            newID = "KAT-" + String.format("%04d", nilai);
            return newID;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    public static String tambahKategori(String namaKategori) {
        try {
            String idKategori = generateIDKategori();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "insert into kategori_barang (id_kategori,nama_kategori,status) values ('" + idKategori + "','" + namaKategori + "','aktif')";
            System.out.println("Query SQL Insert Kategori Barang: "+sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.INSERT_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.INSERT_FAILED_MESSAGE;
        }
    }

//    public static String hapusKategori(Kategori kategori) {
//        try {
//            Statement statement = (Statement) Koneksi.getConnection().createStatement();
//            String sql = "DELETE FROM kategori_barang WHERE id_kategori='" + kategori.getIdKategori() + "'";
//            System.out.println(sql);
//            statement.executeUpdate(sql);
//            statement.close();
//            return "Data Sukses Dihapus";
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "Data Gagal Dihapus";
//        }
//    }
    public static String hapusKategori(Kategori kategori) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "update kategori_barang set status='nonaktif' WHERE id_kategori='" + kategori.getIdKategori() + "' and status='aktif'";
            System.out.println("Query SQL Hapus Kategori Barang: "+sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.DELETE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.DELETE_FAILED_MESSAGE;
        }
    }

    public static String UbahKategori(Kategori kategori, String namaKategori) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE kategori_barang set Nama_Kategori='" + namaKategori + "' where id_kategori='" + kategori.idKategori + "' and status='aktif'";
            System.out.println("Query SQL Update Kategori Barang: "+sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.UPDATE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.UPDATE_FAILED_MESSAGE;
        }
    }
}
