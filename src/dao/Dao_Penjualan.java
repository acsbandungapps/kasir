/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.Dao_Keranjang_Pembelian.list;
import entity.Keranjang;
import entity.Penjualan;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kasirMain.Constant;
import kasirMain.Koneksi;
import util.Function;

/**
 *
 * @author Erdy
 */
public class Dao_Penjualan {

    public static List<Penjualan> list = new ArrayList<>();

//    public static String generateIDPenjualan() {
//        String newID = "";
//        try {
//            String sql = "select * from penjualan order by id_penjualan desc limit 1";
//            Statement st = Koneksi.getConnection().createStatement();
//            ResultSet rs = st.executeQuery(sql);
//            System.out.println("SQL ID Penjualan: " + sql);
//            String serial = "";
//            if (!rs.next()) {
//                serial = "PEN-00000";
//            } else {
//                serial = rs.getString("id_penjualan");
//            }
//            int nilai = Integer.parseInt(serial.substring(4, 9));
//            System.out.println("Nilai ID Penjualan: " + nilai);
//            nilai = nilai + 1;
//            newID = "PEN-" + String.format("%05d", nilai);
//            return newID;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return newID;
//    }
    public static String getDateNowPenjualan() {
        DateFormat datef = new SimpleDateFormat("ddMMyy");
        Date date = new Date();
        String tgl = datef.format(date);
        return tgl;
    }

    public static String generateIDPenjualan() {
        String newID = "";
        String tgl = "";
        try {
            tgl = getDateNowPenjualan();
            String sql = "select * from penjualan order by id_penjualan desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Penjualan: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = tgl + "000000";
            } else {
                serial = rs.getString("id_penjualan");
            }
            int nilai = Integer.parseInt(serial.substring(6, 12));
            System.out.println("Nilai ID Penjualan: " + nilai);
            nilai = nilai + 1;
            newID = tgl + String.format("%06d", nilai);
            return newID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    public static String tambahPenjualan(String total_barang, String total_harga, String uang_dibayar, String kembalian, String status) {
        try {
            String newKembalian = Function.removeSeparator(kembalian);
            String id_penjualan = generateIDPenjualan();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "insert into penjualan (id_penjualan,total_barang,total_harga,uang_dibayar,kembalian,status,tanggal_transaksi,waktu_transaksi) values('" + id_penjualan + "','" + total_barang + "','" + total_harga + "'," + uang_dibayar + ",'" + newKembalian + "','" + status + "','" + Function.getDateNow() + "','" + Function.getTimeNow() + "')";
            System.out.println("Query SQL Insert Penjualan: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            Dao_Penjualan_Detail.insertToPenjualanDetailFromKeranjang(id_penjualan);
            return Constant.INSERT_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.INSERT_FAILED_MESSAGE;
        }
    }

    public static List<Penjualan> selectDataPenjualan() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM `penjualan` ORDER by tanggal_transaksi DESC";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Penjualan p = new Penjualan();
                p.setId_penjualan(res.getString("id_penjualan"));
                p.setTotal_barang(res.getInt("total_barang"));
                p.setTotal_harga(Function.addRp(Function.returnSeparator(res.getString("total_harga"))));
                p.setUang_dibayar(Function.addRp(Function.returnSeparator(res.getString("uang_dibayar"))));
                p.setKembalian(Function.addRp(Function.returnSeparator(res.getString("kembalian"))));
                p.setStatus(res.getString("status"));
                p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                p.setWaktu_transaksi(res.getTime("waktu_transaksi"));

                list.add(p);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan.class.getName()).log(Level.SEVERE, null, e);

        }
        return list;
    }

    public static List<Penjualan> selectDataPenjualanByID(String idPenjualan) {
        try {
            list.clear();
            if (idPenjualan.isEmpty()) {
                selectDataPenjualan();
            } else {
                Statement statement = (Statement) Koneksi.getConnection().createStatement();
                String sql = "SELECT * FROM `penjualan` where id_penjualan like '%" + idPenjualan + "%' ORDER by tanggal_transaksi DESC";
                ResultSet res = statement.executeQuery(sql);
                while (res.next()) {
                    Penjualan p = new Penjualan();
                    p.setId_penjualan(res.getString("id_penjualan"));
                    p.setTotal_barang(res.getInt("total_barang"));
                    p.setTotal_harga(Function.addRp(Function.returnSeparator(res.getString("total_harga"))));
                    p.setUang_dibayar(Function.addRp(Function.returnSeparator(res.getString("uang_dibayar"))));
                    p.setKembalian(Function.addRp(Function.returnSeparator(res.getString("kembalian"))));
                    p.setStatus(res.getString("status"));
                    p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                    p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                    list.add(p);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public static List<Penjualan> selectDataPenjualanByDate(String dateAwal, String dateAkhir) {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM `penjualan` where tanggal_transaksi between '" + dateAwal + "' and '" + dateAkhir + "' ORDER by tanggal_transaksi DESC";
            System.out.println(sql);
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Penjualan p = new Penjualan();
                p.setId_penjualan(res.getString("id_penjualan"));
                p.setTotal_barang(res.getInt("total_barang"));
                p.setTotal_harga(Function.addRp(Function.returnSeparator(res.getString("total_harga"))));
                p.setUang_dibayar(Function.addRp(Function.returnSeparator(res.getString("uang_dibayar"))));
                p.setKembalian(Function.addRp(Function.returnSeparator(res.getString("kembalian"))));
                p.setStatus(res.getString("status"));
                p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                list.add(p);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public static List<Penjualan> selectDataPenjualanByDateandID(String id, String dateAwal, String dateAkhir) {
        try {
            list.clear();
            if (id.isEmpty()) {
                selectDataPenjualan();
            } else {
                Statement statement = (Statement) Koneksi.getConnection().createStatement();
                String sql = "SELECT * FROM `penjualan` where id_penjualan LIKE '%" + id + "%' and tanggal_transaksi between '" + dateAwal + "' and '" + dateAkhir + "' ORDER by tanggal_transaksi DESC";
                System.out.println(sql);
                ResultSet res = statement.executeQuery(sql);
                while (res.next()) {
                    Penjualan p = new Penjualan();
                    p.setId_penjualan(res.getString("id_penjualan"));
                    p.setTotal_barang(res.getInt("total_barang"));
                    p.setTotal_harga(Function.addRp(Function.returnSeparator(res.getString("total_harga"))));
                    p.setUang_dibayar(Function.addRp(Function.returnSeparator(res.getString("uang_dibayar"))));
                    p.setKembalian(Function.addRp(Function.returnSeparator(res.getString("kembalian"))));
                    p.setStatus(res.getString("status"));
                    p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                    p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                    list.add(p);
                }
            }

        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

}
