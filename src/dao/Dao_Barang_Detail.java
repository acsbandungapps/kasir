/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.Dao_Keranjang_Pembelian.list;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kasirMain.Constant;
import kasirMain.Koneksi;
import entity.Barang_Detail;
import entity.Keranjang;
import util.Function;

/**
 *
 * @author Erdy
 */
public class Dao_Barang_Detail {

    public static ArrayList<Barang_Detail> list = new ArrayList<>();
    public static ArrayList<Object> listJoin = new ArrayList<>();
    public static int jmlStok = 0;
    public static int jmlPenjualan = 0;

    public static List<Barang_Detail> findAll() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "select * from barang_detail where status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Barang_Detail bd = new Barang_Detail();
//                bd.setId_barang(res.getString("id_barang"));
//                bd.setId_detail_barang(res.getString("id_detil_barang"));
//                bd.setNama_barang(res.getString("nama_barang"));
//                bd.setHarga_barang("Rp. " + Function.convertFloat(res.getDouble("harga")));
//                bd.setJumlah_pembelian(res.getInt("jumlah_pembelian"));
                list.add(bd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static List<Barang_Detail> selectBarangDetailJoinKategoriAndSatuan() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM `barang_detail` JOIN kategori_barang on barang_detail.id_kategori=kategori_barang.id_kategori join satuan_barang on satuan_barang.id_satuan=barang_detail.id_satuan where barang_detail.status='aktif' and kategori_barang.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Barang_Detail bd = new Barang_Detail();
                bd.setIdDetilBarang(res.getString("id_detil_barang"));
                bd.setIdKategori(res.getString("id_kategori"));
                bd.setNamaKategori(res.getString("nama_kategori"));
                bd.setNamaBarang(res.getString("nama_barang"));
                bd.setJumlahPembelian(res.getInt("jumlah_pembelian"));
                bd.setJumlahStok(res.getInt("jumlah_stok"));
                bd.setHargaBeli("Rp. " + Function.returnSeparator(res.getString("harga_beli")));
                bd.setHargaJual("Rp. " + Function.returnSeparator(res.getString("harga_jual")));
                bd.setLaba("Rp. " + Function.returnSeparator(res.getString("laba")));
                bd.setIdSatuan(res.getString("id_satuan"));
                bd.setNamaSatuan(res.getString("nama_satuan"));
                bd.setStatus(res.getString("status"));
                list.add(bd);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public static List<Barang_Detail> searchBarang(String nama) {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM `barang_detail` JOIN kategori_barang on barang_detail.id_kategori=kategori_barang.id_kategori join satuan_barang on satuan_barang.id_satuan=barang_detail.id_satuan where barang_detail.nama_barang LIKE '%" + nama + "%' and barang_detail.jumlah_stok>0 and barang_detail.status='aktif' and kategori_barang.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Barang_Detail bd = new Barang_Detail();
                bd.setIdDetilBarang(res.getString("id_detil_barang"));
                bd.setIdKategori(res.getString("id_kategori"));
                bd.setNamaKategori(res.getString("nama_kategori"));
                bd.setNamaBarang(res.getString("nama_barang"));
                bd.setJumlahPembelian(res.getInt("jumlah_pembelian"));
                bd.setJumlahStok(res.getInt("jumlah_stok"));
                bd.setHargaBeli("Rp. " + Function.returnSeparator(res.getString("harga_beli")));
                bd.setHargaJual("Rp. " + Function.returnSeparator(res.getString("harga_jual")));
                bd.setLaba("Rp. " + Function.returnSeparator(res.getString("laba")));
                bd.setIdSatuan(res.getString("id_satuan"));
                bd.setNamaSatuan(res.getString("nama_satuan"));
                bd.setStatus(res.getString("status"));
                list.add(bd);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Barang_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public static List<Barang_Detail> selectBarangDetailExceedZero() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM `barang_detail` JOIN kategori_barang on barang_detail.id_kategori=kategori_barang.id_kategori join satuan_barang on satuan_barang.id_satuan=barang_detail.id_satuan where barang_detail.jumlah_stok>0 and barang_detail.status='aktif'";
            //String a=statement.executeQuery(sql).getNString("id_detil_barang");
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Barang_Detail bd = new Barang_Detail();
                bd.setIdDetilBarang(res.getString("id_detil_barang"));
                bd.setIdKategori(res.getString("id_kategori"));
                bd.setNamaKategori(res.getString("nama_kategori"));
                bd.setNamaBarang(res.getString("nama_barang"));
                bd.setJumlahPembelian(res.getInt("jumlah_pembelian"));
                bd.setJumlahStok(res.getInt("jumlah_stok"));
                bd.setHargaBeli("Rp. " + Function.returnSeparator(res.getString("harga_beli")));
                bd.setHargaJual("Rp. " + Function.returnSeparator(res.getString("harga_jual")));
                bd.setLaba("Rp. " + Function.returnSeparator(res.getString("laba")));
                bd.setIdSatuan(res.getString("id_satuan"));
                bd.setNamaSatuan(res.getString("nama_satuan"));
                bd.setStatus(res.getString("status"));
                list.add(bd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Barang_Detail.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static String generateIDBarangDetail() {
        String newID = "";
        try {
            String sql = "select * from barang_detail order by id_detil_barang desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Barang Detail: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = "BRG-00000";
            } else {
                serial = rs.getString("id_detil_barang");
            }
            int nilai = Integer.parseInt(serial.substring(4, 9));
            System.out.println("Nilai ID Detil Barang: " + nilai);
            nilai = nilai + 1;
            newID = "BRG-" + String.format("%05d", nilai);
            return newID;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    //id_detil_barang
    //id_kategori
    //nama_barang
    //jumlah_pembelian
    //jumlah_stok
    //harga_beli
    //harga_jual
    //laba
    //id_satuan
    //status
    public static String tambahBarangDetail(String id_kategori, String namaBarang, int jumlah_pembelian, int stok_barang, Double harga_beli, Double harga_jual, String id_satuan, Double laba) {
        try {
            String id_barang_detail = generateIDBarangDetail();
            //String id_barang_stok = generateIDBarangStok();
            System.out.println("ID Barang Detail: " + id_barang_detail);
            //System.out.println("ID Barang Stok: " + id_barang_stok);
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            //String sql = "insert into barang_detail (id_detil_barang,id_kategori,nama_barang,jumlah_pembelian,id_satuan,id_stok_barang,status) values ('" + id_barang_detail + "','" + id_kategori + "','" + namaBarang + "','" + 0 + "','" + id_satuan + "','" + id_barang_stok + "','aktif')";
            String sql = "insert into barang_detail (id_detil_barang,id_kategori,nama_barang,jumlah_pembelian,jumlah_stok,harga_beli,harga_jual,laba,id_satuan,status) values ('" + id_barang_detail + "','" + id_kategori + "','" + namaBarang + "',0,'" + stok_barang + "','" + harga_beli + "','" + harga_jual + "','" + laba + "','" + id_satuan + "','aktif')";
            System.out.println("Query SQL Insert Barang_Detail: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            //Dao_Barang_Stok.tambahBarangStok(id_barang_stok, id_kategori, id_barang_detail, namaBarang, stok_barang, harga_beli, harga_jual, laba, id_satuan);
            return Constant.INSERT_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.INSERT_FAILED_MESSAGE;
        }
    }

    public static String UbahBarangDetail(String idDetilBarang, String idKategori, String namaBarang, String idSatuan, int jumlah_stok, Double harga_beli, Double harga_jual, Double laba) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE barang_detail set id_kategori='" + idKategori + "',nama_barang='" + namaBarang + "',jumlah_stok='" + jumlah_stok + "',harga_beli='" + harga_beli + "',harga_jual='" + harga_jual + "',laba='" + laba + "',id_satuan='" + idSatuan + "' where id_detil_barang='" + idDetilBarang + "' and status='aktif'";
            System.out.println("Query SQL Ubah Barang Detail: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            //Dao_Barang_Stok.UbahBarangStok(idDetilBarang, idKategori, namaBarang, idSatuan, jumlah_stok, harga_beli, harga_jual, laba);
            return Constant.UPDATE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.UPDATE_FAILED_MESSAGE;
        }
    }

    public static String HapusBarangDetail(String idDetilBarang) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE barang_detail set status='nonaktif' where id_detil_barang='" + idDetilBarang + "' and status='aktif'";
            System.out.println("Query SQL Hapus Barang Detail: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            //Dao_Barang_Stok.HapusBarangStok(idDetilBarang, idStokBarang);
            return Constant.DELETE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.DELETE_SUCCESS_MESSAGE;
        }
    }

//    public static int getStock(String id) {
//        System.out.println(id);
//        int jmlStok = 0;
//        try {
//            list.clear();
//            Statement statement = (Statement) Koneksi.getConnection().createStatement();
//            //String sql = "SELECT jumlah_stok FROM `barang_detail` where barang_detail.id_detil_barang ='" + id + "' and barang_detail.jumlah_stok>0 and barang_detail.status='aktif'";
//            String sql="SELECT jumlah_stok FROM `barang_detail` where barang_detail.id_detil_barang ='"+id+"' and barang_detail.jumlah_stok>0 and barang_detail.status='aktif'";
//            System.out.println(sql);
////            String a=statement.executeQuery(sql).getNString("id_detil_barang");
//            jmlStok = statement.executeQuery(sql).getInt("jumlah_stok");
//            return jmlStok;
//        } catch (Exception ex) {
//            Logger.getLogger(Dao_Barang_Detail.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//        return jmlStok;
//    }
    public static int getStock(String id) {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT jumlah_stok FROM `barang_detail` where barang_detail.id_detil_barang ='" + id + "' and barang_detail.jumlah_stok>0 and barang_detail.status='aktif'";
            //String a=statement.executeQuery(sql).getNString("id_detil_barang");
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                jmlStok = res.getInt("jumlah_stok");
//                Barang_Detail bd = new Barang_Detail();
//                bd.setIdDetilBarang(res.getString("id_detil_barang"));
//                bd.setIdKategori(res.getString("id_kategori"));
//                bd.setNamaKategori(res.getString("nama_kategori"));
//                bd.setNamaBarang(res.getString("nama_barang"));
//                bd.setJumlahPembelian(res.getInt("jumlah_pembelian"));
//                bd.setJumlahStok(res.getInt("jumlah_stok"));
//                bd.setHargaBeli(res.getString("harga_beli"));
//                bd.setHargaJual(res.getString("harga_jual"));
//                bd.setLaba(res.getString("laba"));
//                bd.setIdSatuan(res.getString("id_satuan"));
//                bd.setNamaSatuan(res.getString("nama_satuan"));
//                bd.setStatus(res.getString("status"));
//                list.add(bd);
                return jmlStok;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Barang_Detail.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return jmlStok;
    }

    public static String potongStok(String id, String jumlah) {
        try {
            int Stok = getStock(id);
            int sisaStok = Stok - Integer.parseInt(jumlah);
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "update barang_detail set jumlah_stok=" + sisaStok + " WHERE id_detil_barang='" + id + "' and barang_detail.status='aktif'";
            System.out.println("Query SQL Ubah Barang Detail: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.CUT_STOK_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.CUT_STOK_FAILED_MESSAGE;
        }
    }

    public static String kembalikanStok(String id, String jumlah) {
        try {
            int Stok = getStock(id);
            int sisaStok = Stok + Integer.parseInt(jumlah);
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "update barang_detail set jumlah_stok=" + sisaStok + " WHERE id_detil_barang='" + id + "' and barang_detail.status='aktif'";
            System.out.println("Query SQL Ubah Barang Detail: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            return Constant.ADD_STOK_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.ADD_STOK_FAILED_MESSAGE;
        }
    }

    public static String UbahJumlahPenjualanBarang(String idDetilBarang, int jumlahPembelian) {
        try {
            int jmlPembelian = getTotalPenjualan(idDetilBarang);
            int total = jmlPembelian + jumlahPembelian;
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "UPDATE barang_detail set jumlah_pembelian='" + total + "' where id_detil_barang='" + idDetilBarang + "' and status='aktif'";
            System.out.println("Query SQL Tambah Penjualan Barang: " + sql);
            statement.executeUpdate(sql);
            statement.close();
            //Dao_Barang_Stok.UbahBarangStok(idDetilBarang, idKategori, namaBarang, idSatuan, jumlah_stok, harga_beli, harga_jual, laba);
            return Constant.UPDATE_SUCCESS_MESSAGE;
        } catch (Exception e) {
            e.printStackTrace();
            return Constant.UPDATE_FAILED_MESSAGE;
        }
    }

    public static int getTotalPenjualan(String id) {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT jumlah_pembelian FROM `barang_detail` where barang_detail.id_detil_barang ='" + id + "'and barang_detail.status='aktif'";
            //String a=statement.executeQuery(sql).getNString("id_detil_barang");
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                jmlStok = res.getInt("jumlah_pembelian");
                return jmlStok;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_Barang_Detail.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return jmlStok;
    }

}
