/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.Dao_Barang_Detail.list;
import static dao.Dao_Penjualan.list;
import static dao.Dao_Penjualan.selectDataPenjualan;
import entity.Barang_Detail;
import entity.Keranjang;
import entity.Penjualan;
import entity.Penjualan_Detail;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kasirMain.Constant;
import kasirMain.Koneksi;
import util.Function;

/**
 *
 * @author Erdy
 */
public class Dao_Penjualan_Detail {

    public static List<Keranjang> list = new ArrayList<>();
    public static List<Penjualan_Detail> listP = new ArrayList<>();

    public static void insertToPenjualanDetailFromKeranjang(String idPenjualan) {
        List<Keranjang> listKeranjang = new ArrayList<Keranjang>();
        listKeranjang = dao.Dao_Penjualan_Detail.selectKeranjangJoinBarangDetail();
        for (int i = 0; i < listKeranjang.size(); i++) {
            String id_detil_penjualan = "";
            id_detil_penjualan = generateIDDetilPenjualan();
            System.out.println("ID Detil Penjualan: " + id_detil_penjualan);
            System.out.println("ID Penjualan: " + idPenjualan);
            System.out.println("ID Kategori Barang: " + listKeranjang.get(i).getIdKategoriBarang());
            System.out.println("ID Detil Barang: " + listKeranjang.get(i).getIdDetilBarang());
            System.out.println("Tanggal Transaksi: " + listKeranjang.get(i).getTglTransaksi().toString());
            System.out.println("Waktu Transaksi: " + listKeranjang.get(i).getWaktuTransaksi());
            System.out.println("Harga: " + listKeranjang.get(i).getHargaJual());
            System.out.println("Jumlah: " + String.valueOf(listKeranjang.get(i).getJumlah_beli()));
            System.out.println("Harga Beli " + listKeranjang.get(i).getHargaBeli());
            System.out.println("Laba " + listKeranjang.get(i).getLaba());
            insertPenjualanDetail(id_detil_penjualan, idPenjualan, listKeranjang.get(i).getIdKategoriBarang(), listKeranjang.get(i).getIdDetilBarang(), listKeranjang.get(i).getTglTransaksi(), listKeranjang.get(i).getWaktuTransaksi(), listKeranjang.get(i).getHargaJual(), listKeranjang.get(i).getHargaBeli(), listKeranjang.get(i).getLaba(), String.valueOf(listKeranjang.get(i).getJumlah_beli()), Constant.STATUS);
        }
    }

    public static List<Keranjang> selectKeranjangJoinBarangDetail() {
        try {
            list.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT * FROM keranjang_penjualan join barang_detail on keranjang_penjualan.id_detil_barang=barang_detail.id_detil_barang \n"
                    + "join kategori_barang on kategori_barang.id_kategori=barang_detail.id_kategori JOIN satuan_barang ON satuan_barang.id_satuan=barang_detail.id_satuan \n"
                    + "WHERE keranjang_penjualan.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Keranjang k = new Keranjang();
                k.setIdKeranjang(res.getString("id_keranjang"));
                k.setIdKategoriBarang(res.getString("id_kategori"));
                k.setNamaKategoriBarang(res.getString("nama_kategori"));
                k.setIdDetilBarang(res.getString("id_detil_barang"));
                k.setNamaBarang(res.getString("nama_barang"));
                k.setJumlahPembelian(res.getInt("jumlah_pembelian"));
                k.setJumlahStok(res.getInt("jumlah_stok"));
                k.setJumlah_beli(res.getInt("jumlah_beli"));
                k.setHargaBeli(res.getString("harga_beli"));
                k.setHargaJual(res.getString("harga_jual"));
                k.setTotal(res.getString("total"));
                k.setLaba(res.getString("laba"));
                k.setIdSatuan(res.getString("id_satuan"));
                k.setNamaSatuan(res.getString("nama_satuan"));
                k.setTglTransaksi(res.getString("tgl_transaksi"));
                k.setWaktuTransaksi(res.getTime("waktu_transaksi"));
                k.setKeterangan(res.getString("keterangan"));
                k.setStatus(res.getString("status"));
                list.add(k);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public static void insertPenjualanDetail(String id_detil_penjualan, String idPenjualan, String id_kategori_barang, String id_detil_barang, String tanggal_transaksi, Time waktu_transaksi, String harga, String hargaBeli, String laba, String jumlah, String status) {
        try {
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "insert into penjualan_detail (id_detil_penjualan,id_penjualan,id_barang,id_detil_barang,tanggal_transaksi,waktu_transaksi,harga,harga_beli,laba,jumlah,status) values('" + id_detil_penjualan + "','" + idPenjualan + "','" + id_kategori_barang + "','" + id_detil_barang + "','" + tanggal_transaksi + "','" + waktu_transaksi + "'," + harga + "," + hargaBeli + "," + laba + "," + jumlah + ",'" + status + "')";
            System.out.println("Query SQL Insert Penjualan Detail: " + sql);
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    public static String generateIDDetilPenjualan() {
//        String newID = "";
//        try {
//            String sql = "select * from penjualan_detail order by id_detil_penjualan desc limit 1";
//            Statement st = Koneksi.getConnection().createStatement();
//            ResultSet rs = st.executeQuery(sql);
//            System.out.println("SQL ID Detil Penjualan: " + sql);
//            String serial = "";
//            if (!rs.next()) {
//                serial = "DET-00000";
//            } else {
//                serial = rs.getString("id_detil_penjualan");
//            }
//            int nilai = Integer.parseInt(serial.substring(4, 9));
//            System.out.println("Nilai ID Detil Penjualan: " + nilai);
//            nilai = nilai + 1;
//            newID = "DET-" + String.format("%05d", nilai);
//            return newID;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return newID;
//    }
    public static String generateIDDetilPenjualan() {
        String newID = "";
        String tgl = "";
        try {
            tgl = Dao_Penjualan.getDateNowPenjualan();
            String sql = "select * from penjualan_detail order by id_detil_penjualan desc limit 1";
            Statement st = Koneksi.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println("SQL ID Detil Penjualan: " + sql);
            String serial = "";
            if (!rs.next()) {
                serial = "D-" + tgl + "000000";
            } else {
                serial = rs.getString("id_detil_penjualan");
            }
            int nilai = Integer.parseInt(serial.substring(6, 12));
            System.out.println("Nilai ID Detil Penjualan: " + nilai);
            nilai = nilai + 1;
            newID = tgl + String.format("%06d", nilai);
            return newID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    public static List<Penjualan_Detail> selectPenjualanDetailJoinBarangDetail(String id) {
        try {
            listP.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT penjualan_detail.id_penjualan, penjualan_detail.id_detil_penjualan,penjualan_detail.id_detil_barang,barang_detail.nama_barang,penjualan_detail.harga,penjualan_detail.jumlah,penjualan_detail.tanggal_transaksi,penjualan_detail.waktu_transaksi FROM penjualan_detail join barang_detail ON penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.id_penjualan='" + id + "' and barang_detail.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Penjualan_Detail p = new Penjualan_Detail();
                p.setId_penjualan(res.getString("id_penjualan"));
                p.setId_detil_penjualan(res.getString("id_detil_penjualan"));
                p.setId_detil_barang(res.getString("id_detil_barang"));
                p.setNamaBarang(res.getString("nama_barang"));
                p.setHarga(Function.addRp(Function.returnSeparator(res.getString("harga"))));
                p.setJumlah(res.getInt("jumlah"));
                p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                p.setTotal(Function.addRp(Function.returnSeparator(String.valueOf(Integer.parseInt(res.getString("harga")) * res.getInt("jumlah")))));

                listP.add(p);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return listP;
    }

    public static List<Penjualan_Detail> searchDetailPenjualan(String id) {
        try {
            listP.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT penjualan_detail.id_penjualan, penjualan_detail.id_detil_penjualan,penjualan_detail.id_detil_barang,barang_detail.nama_barang,penjualan_detail.harga,penjualan_detail.jumlah,penjualan_detail.tanggal_transaksi,penjualan_detail.waktu_transaksi FROM penjualan_detail join barang_detail ON penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.id_penjualan like '%" + id + "%' and barang_detail.status='aktif'";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Penjualan_Detail p = new Penjualan_Detail();
                p.setId_penjualan(res.getString("id_penjualan"));
                p.setId_detil_penjualan(res.getString("id_detil_penjualan"));
                p.setId_detil_barang(res.getString("id_detil_barang"));
                p.setNamaBarang(res.getString("nama_barang"));
                p.setHarga(Function.addRp(Function.returnSeparator(res.getString("harga"))));
                p.setJumlah(res.getInt("jumlah"));
                p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                p.setTotal(Function.addRp(Function.returnSeparator(String.valueOf(Integer.parseInt(res.getString("harga")) * res.getInt("jumlah")))));
                listP.add(p);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return listP;
    }

    public static List<Penjualan_Detail> selectAllDetailPenjualan() {
        try {
            listP.clear();
            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT penjualan_detail.id_penjualan, penjualan_detail.id_detil_penjualan,penjualan_detail.id_detil_barang,barang_detail.nama_barang,penjualan_detail.harga,penjualan_detail.jumlah,penjualan_detail.tanggal_transaksi,penjualan_detail.waktu_transaksi FROM penjualan_detail join barang_detail ON penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where barang_detail.status='aktif' order by tanggal_transaksi DESC";
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Penjualan_Detail p = new Penjualan_Detail();
                p.setId_penjualan(res.getString("id_penjualan"));
                p.setId_detil_penjualan(res.getString("id_detil_penjualan"));
                p.setId_detil_barang(res.getString("id_detil_barang"));
                p.setNamaBarang(res.getString("nama_barang"));
                p.setHarga(Function.addRp(Function.returnSeparator(res.getString("harga"))));
                p.setJumlah(res.getInt("jumlah"));
                p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                p.setTotal(Function.addRp(Function.returnSeparator(String.valueOf(Integer.parseInt(res.getString("harga")) * res.getInt("jumlah")))));
                listP.add(p);
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return listP;
    }

    public static List<Penjualan_Detail> selectDataPenjualanDetailByID(String idPenjualan) {
        try {
            listP.clear();
            if (idPenjualan.isEmpty()) {
                selectDataPenjualan();
            } else {
                Statement statement = (Statement) Koneksi.getConnection().createStatement();
                String sql = "SELECT penjualan_detail.id_penjualan, penjualan_detail.id_detil_penjualan,penjualan_detail.id_detil_barang,barang_detail.nama_barang,penjualan_detail.harga,penjualan_detail.jumlah,penjualan_detail.tanggal_transaksi,penjualan_detail.waktu_transaksi FROM penjualan_detail join barang_detail ON penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where barang_detail.status='aktif' and id_penjualan like '%" + idPenjualan + "%' order by tanggal_transaksi DESC";
                //String sql = "SELECT * FROM `penjualan_detail` where id_penjualan like '%" + idPenjualan + "%' ORDER by tanggal_transaksi DESC";
                System.out.println(sql);
                ResultSet res = statement.executeQuery(sql);
                while (res.next()) {
                    Penjualan_Detail p = new Penjualan_Detail();
                    p.setId_penjualan(res.getString("id_penjualan"));
                    p.setId_detil_penjualan(res.getString("id_detil_penjualan"));
                    p.setId_detil_barang(res.getString("id_detil_barang"));
                    p.setNamaBarang(res.getString("nama_barang"));
                    p.setHarga(Function.addRp(Function.returnSeparator(res.getString("harga"))));
                    p.setJumlah(res.getInt("jumlah"));
                    p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                    p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                    p.setTotal(Function.addRp(Function.returnSeparator(String.valueOf(Integer.parseInt(res.getString("harga")) * res.getInt("jumlah")))));
                    listP.add(p);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return listP;
    }

    public static List<Penjualan_Detail> selectDataPenjualanDetailByDate(String dateAwal, String dateAkhir) {
        try {
            listP.clear();

            Statement statement = (Statement) Koneksi.getConnection().createStatement();
            String sql = "SELECT penjualan_detail.id_penjualan, penjualan_detail.id_detil_penjualan,penjualan_detail.id_detil_barang,barang_detail.nama_barang,penjualan_detail.harga,penjualan_detail.jumlah,penjualan_detail.tanggal_transaksi,penjualan_detail.waktu_transaksi FROM penjualan_detail join barang_detail ON penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where barang_detail.status='aktif' and tanggal_transaksi between '" + dateAwal + "' and '" + dateAkhir + "' order by tanggal_transaksi DESC";
            System.out.println(sql);
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                Penjualan_Detail p = new Penjualan_Detail();
                p.setId_penjualan(res.getString("id_penjualan"));
                p.setId_detil_penjualan(res.getString("id_detil_penjualan"));
                p.setId_detil_barang(res.getString("id_detil_barang"));
                p.setNamaBarang(res.getString("nama_barang"));
                p.setHarga(Function.addRp(Function.returnSeparator(res.getString("harga"))));
                p.setJumlah(res.getInt("jumlah"));
                p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                p.setTotal(Function.addRp(Function.returnSeparator(String.valueOf(Integer.parseInt(res.getString("harga")) * res.getInt("jumlah")))));
                listP.add(p);
            }

        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return listP;
    }

    public static List<Penjualan_Detail> selectDataPenjualanByDateandID(String idPenjualan,String dateAwal, String dateAkhir) {
        try {
            listP.clear();
            if (idPenjualan.isEmpty()) {
                selectDataPenjualan();
            } else {
                Statement statement = (Statement) Koneksi.getConnection().createStatement();
                String sql = "SELECT penjualan_detail.id_penjualan, penjualan_detail.id_detil_penjualan,penjualan_detail.id_detil_barang,barang_detail.nama_barang,penjualan_detail.harga,penjualan_detail.jumlah,penjualan_detail.tanggal_transaksi,penjualan_detail.waktu_transaksi FROM penjualan_detail join barang_detail ON penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where barang_detail.status='aktif' and id_penjualan like '%" + idPenjualan + "%' order by tanggal_transaksi DESC";
                System.out.println(sql);
                ResultSet res = statement.executeQuery(sql);
                while (res.next()) {
                    Penjualan_Detail p = new Penjualan_Detail();
                    p.setId_penjualan(res.getString("id_penjualan"));
                    p.setId_detil_penjualan(res.getString("id_detil_penjualan"));
                    p.setId_detil_barang(res.getString("id_detil_barang"));
                    p.setNamaBarang(res.getString("nama_barang"));
                    p.setHarga(Function.addRp(Function.returnSeparator(res.getString("harga"))));
                    p.setJumlah(res.getInt("jumlah"));
                    p.setTanggal_transaksi(Function.returnFormattedDate(res.getString("tanggal_transaksi")));
                    p.setWaktu_transaksi(res.getTime("waktu_transaksi"));
                    p.setTotal(Function.addRp(Function.returnSeparator(String.valueOf(Integer.parseInt(res.getString("harga")) * res.getInt("jumlah")))));
                    listP.add(p);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Dao_Penjualan_Detail.class.getName()).log(Level.SEVERE, null, e);
        }
        return listP;
    }

}
