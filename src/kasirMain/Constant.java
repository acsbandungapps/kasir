package kasirMain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Erdy
 */
public class Constant {

    public static String INSERT_SUCCESS_MESSAGE = "Data Berhasil Dimasukkan";
    public static String INSERT_FAILED_MESSAGE = "Error Memasukkan Data";
    public static String UPDATE_SUCCESS_MESSAGE = "Data Berhasil Diubah";
    public static String UPDATE_FAILED_MESSAGE = "Error Mengubah Data";
    public static String DELETE_SUCCESS_MESSAGE = "Data Sukses Dihapus";
    public static String DELETE_FAILED_MESSAGE = "Data Gagal Dihapus";
    public static String IS_EMPTY = "Mohon Lengkapi Semua Data";
    public static String WRONG_SELLING_PRICE = "Harga Jual Harus Lebih Besar dari Harga Beli";
    public static String PRICE_CANNOT_LESS_THAN_ZERO = "Harga Tidak Boleh Kurang dari 0";
    public static String INPUT_CANNOT_ZERO = "Input Tidak Boleh 0";
    public static String STOCK_NOT_ENOUGH = "Stok Tidak Cukup";
    public static String BYNAME = "berdasarkan nama";
    public static String BYCATEGORY = "berdasarkan kategori";
    public static String STATUS = "Lunas";
    public static String EMPTY_KERANJANG = "Keranjang Kosong";
    public static String FILLED_KERANJANG = "Keranjang Ada";

    public static String MESSAGE = "Pesan";
    public static String SURE_TO_PAY = "Anda Yakin Ingin Membayar?";
    public static String MONEY_NOT_ENOUGH = "Uang yang Dibayar Lebih Kecil dari Total Belanjaan";
    public static String PLEASE_FILL_FIELD_MONEY = "Mohon Isi Besar Uang Yang Dibayar";

    //STOK BARANG_DETAIL
    public static String CUT_STOK_SUCCESS_MESSAGE = "Stok Sudah Dikurangi";
    public static String CUT_STOK_FAILED_MESSAGE = "Stok Gagal Dikurangi";
    public static String ADD_STOK_SUCCESS_MESSAGE = "Stok Sudah Ditambah";
    public static String ADD_STOK_FAILED_MESSAGE = "Stok Gagal Ditambah";

}
