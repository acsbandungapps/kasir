/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kasirMain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erdy
 */
public class Koneksi {
    private static Connection connection;
    public static Connection getConnection() {
        if (connection == null) {
            try {
                DriverManager.registerDriver(new com.mysql.jdbc.Driver());
                connection = DriverManager.getConnection("jdbc:mysql://172.168.2.65:3306/kasir", "admin", "root");
                //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/kasir", "root", "");
                //connection = DriverManager.getConnection("jdbc:mysql://10.10.10.200:3306/wahanasolusi", "admin", "root");
                //connection = DriverManager.getConnection("jdbc:mysql://10.10.10.23:3306/wahanasolusi", "admin", "root");
                //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/absensi", "root", "");
            } catch (SQLException ex) {
                Logger.getLogger(Koneksi.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Ini errornya: " + ex.getMessage());
            }
        }
        return connection;
    }
}
