/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.LocalDate;

/**
 *
 * @author Erdy
 */
public class CreateExcel {

    static String TotalHarga;
    static String pesan = "";

    static List<LocalDate> listTanggal = new ArrayList<>();

//    public static void main(String[] args) throws ParseException, Exception {
//        System.out.println(createExcelPerIdandDate("140217000001","2017-02-14","2017-02-15"));
//    }

      public static String createExcelPerIdandDate(String idPenjualan,String dateAwal,String dateAkhir) {
//         String idPenjualan = "140217000001";
//        String dateAwal = "2017-02-14";
//        String dateAkhir = "2017-02-15";
        String kategori = "ID Penjualan";
        DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        String[] partsAwal = dateAwal.split("-");
        int part1Awal = Integer.parseInt(partsAwal[0]);
        int part2Awal = Integer.parseInt(partsAwal[1]);
        int part3Awal = Integer.parseInt(partsAwal[2]);

        String[] partsAkhir = dateAkhir.split("-");
        int part1Akhir = Integer.parseInt(partsAkhir[0]);
        int part2Akhir = Integer.parseInt(partsAkhir[1]);
        int part3Akhir = Integer.parseInt(partsAkhir[2]);

        System.out.println(part1Awal);
        System.out.println(part2Awal);
        System.out.println(part3Awal);
        System.out.println(part1Akhir);
        System.out.println(part2Akhir);
        System.out.println(part3Akhir);

        for (LocalDate ld = new LocalDate(part1Awal, part2Awal, part3Awal); ld.isBefore(new LocalDate(part1Akhir, part2Akhir, part3Akhir)) || ld.isEqual(new LocalDate(part1Akhir, part2Akhir, part3Akhir)); ld = ld.plusDays(1)) {
            // System.out.println(ld);
            listTanggal.add(ld);
        }

        String excelFileName = System.getProperty("user.home") + "/Desktop/Data Penjualan " + dateFormat.format(date) + ".xls";
        HSSFWorkbook wb = new HSSFWorkbook();

        for (LocalDate ld : listTanggal) {
            // System.out.println(ld);
            System.out.println("INIH: " + ld.toString());
            String sheetName = Function.returnFormattedDate(ld.toString());

            HSSFSheet sheet = wb.createSheet(sheetName);
            //untuk baris keberapa
            Row detailTitleRow = sheet.createRow(0);

            //untuk kolom keberapa
            Cell detailTitleCell = detailTitleRow.createCell(0);

            CellStyle detailTitleCellStyle = sheet.getWorkbook().createCellStyle();
            detailTitleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            detailTitleCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            detailTitleCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            detailTitleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            Font fontDetailTitle = sheet.getWorkbook().createFont();
            fontDetailTitle.setFontHeightInPoints((short) 14);
            fontDetailTitle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
            detailTitleCellStyle.setFont(fontDetailTitle);

            CellStyle detailTitleStyle = sheet.getWorkbook().createCellStyle();
            Font fontDetailTitleStyle = sheet.getWorkbook().createFont();
            fontDetailTitleStyle.setFontHeightInPoints((short) 20);
            fontDetailTitleStyle.setBoldweight(Font.BOLDWEIGHT_BOLD);
            detailTitleStyle.setFont(fontDetailTitleStyle);

            detailTitleCell.setCellStyle(detailTitleStyle);
//            detailTitleCell.setCellValue("Data Penjualan " + Function.returnFormattedDate(dateAwal) + " - " + Function.returnFormattedDate(dateAkhir));
            detailTitleCell.setCellValue("Data Penjualan " + sheetName);
            Row headerRow = sheet.createRow(2);
            Cell IDPenjualanHeaderCell = headerRow.createCell(0);
            Cell TglTransaksiHeaderCell = headerRow.createCell(1);
            Cell NamaBarangHeaderCell = headerRow.createCell(2);
            Cell HargaHeaderCell = headerRow.createCell(3);
            Cell BanyakBarangHeaderCell = headerRow.createCell(4);
            Cell TotalHargaHeaderCell = headerRow.createCell(5);
            //Cell TotalPenjualanHeaderCell = headerRow.createCell(5);

            IDPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);
            TglTransaksiHeaderCell.setCellStyle(detailTitleCellStyle);
            NamaBarangHeaderCell.setCellStyle(detailTitleCellStyle);
            HargaHeaderCell.setCellStyle(detailTitleCellStyle);
            BanyakBarangHeaderCell.setCellStyle(detailTitleCellStyle);
            TotalHargaHeaderCell.setCellStyle(detailTitleCellStyle);
            //TotalPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);

            IDPenjualanHeaderCell.setCellValue("ID Penjualan");
            TglTransaksiHeaderCell.setCellValue("Tanggal Transaksi");
            NamaBarangHeaderCell.setCellValue("Nama Barang");
            HargaHeaderCell.setCellValue("Harga");
            BanyakBarangHeaderCell.setCellValue("Banyak Barang");
            TotalHargaHeaderCell.setCellValue("Total Harga");

            String sql = "SELECT penjualan_detail.id_penjualan,penjualan_detail.id_detil_barang,penjualan_detail.harga,penjualan_detail.jumlah,barang_detail.nama_barang, penjualan.total_harga,penjualan_detail.tanggal_transaksi FROM `penjualan_detail` join penjualan on penjualan.id_penjualan=penjualan_detail.id_penjualan join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.id_penjualan like '"+idPenjualan+"' and penjualan_detail.tanggal_transaksi ='"+ld.toString()+"' ORDER by penjualan_detail.id_penjualan DESC";
            String sqlTotal = "SELECT sum(total_harga) as Total FROM `penjualan` where id_penjualan like '"+idPenjualan+"' and tanggal_transaksi ='"+ld.toString()+"'";
            System.out.println(sql);
            System.out.println(sqlTotal);
            PreparedStatement ps;
            PreparedStatement psTotal;

            try {
                ps = kasirMain.Koneksi.getConnection().prepareStatement(sql);
                psTotal = kasirMain.Koneksi.getConnection().prepareStatement(sqlTotal);
                ResultSet resultSet = ps.executeQuery();
                ResultSet resultSetTotal = psTotal.executeQuery();
                int row = 3;

                while (resultSetTotal.next()) {
                    TotalHarga = resultSetTotal.getString("Total");
                }
                while (resultSet.next()) {
                    String IDPenjualan = resultSet.getString("id_penjualan");
                    String NamaBarang = resultSet.getString("nama_barang");
                    //String Harga = Function.addRp(Function.returnSeparator(resultSet.getString("harga")));
                    String Harga = resultSet.getString("harga");
                    String Jumlah = resultSet.getString("jumlah");
                    int hasil = Integer.parseInt(Harga) * Integer.parseInt(Jumlah);
                    String tglTransaksi = resultSet.getString("tanggal_transaksi");

                    Row dataRow = sheet.createRow(row);

                    Cell dataIDPenjualanCell = dataRow.createCell(0);
                    dataIDPenjualanCell.setCellValue(IDPenjualan);
                    dataIDPenjualanCell.setCellStyle(detailTitleCellStyle);

                    Cell dataTglTransaksiCell = dataRow.createCell(1);
                    dataTglTransaksiCell.setCellValue(tglTransaksi);
                    dataTglTransaksiCell.setCellStyle(detailTitleCellStyle);

                    Cell dataNamaBarangCell = dataRow.createCell(2);
                    dataNamaBarangCell.setCellValue(NamaBarang);
                    dataNamaBarangCell.setCellStyle(detailTitleCellStyle);

                    Cell dataHargaCell = dataRow.createCell(3);
                    dataHargaCell.setCellValue(Function.returnSeparator(Harga));
                    dataHargaCell.setCellStyle(detailTitleCellStyle);

                    Cell dataJumlahTransaksiCell = dataRow.createCell(4);
                    dataJumlahTransaksiCell.setCellValue(Jumlah);
                    dataJumlahTransaksiCell.setCellStyle(detailTitleCellStyle);

                    Cell dataHasilCell = dataRow.createCell(5);
                    dataHasilCell.setCellValue(Function.returnSeparator(String.valueOf(hasil)));
                    dataHasilCell.setCellStyle(detailTitleCellStyle);

                    row = row + 1;
                }
                Row dataRow = sheet.createRow(row);
                CellStyle style = wb.createCellStyle();
                style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
                style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
                Font font = wb.createFont();
                font.setColor(HSSFColor.RED.index);
                font.setFontHeightInPoints((short) 20);
                font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
                style.setFont(font);

                Cell CellTitleTotalHarga = dataRow.createCell(4);
                CellTitleTotalHarga.setCellValue("Total Harga");
                CellTitleTotalHarga.setCellStyle(detailTitleCellStyle);

                Cell dataTotalHargaCell = dataRow.createCell(5);
                dataTotalHargaCell.setCellValue(TotalHarga);
                dataTotalHargaCell.setCellStyle(style);

                Cell CellGenerated = dataRow.createCell(0);
                CellGenerated.setCellValue("Generated Time: " + Function.returnDateDayName(Function.getDateNow()) + " " + Function.getTimeNow());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
        }

        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(excelFileName);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            System.out.println("Excel Berhasil Dibuat");
            pesan = "Excel Berhasil Dibuat";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Excel Berhasil Dibuat");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Mohon Tutup Terlebih Dahulu Excel Anda");
            pesan = "Mohon Tutup Terlebih Dahulu Excel Anda";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Mohon Tutup Terlebih Dahulu Excel Anda");
        } catch (IOException ex) {
            //JOptionPane.showMessageDialog(this, "File Excel Gagal Dibuat");
            // Logger.getLogger(MenuDataAbsen.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("File Excel Gagal Dibuat");
            pesan = "File Excel Gagal Dibuat";
            return pesan;

        }
    }
      
      
    public static String createExcelperDate(String dateAwal,String dateAkhir) {
//        String idPenjualan = "310117000001";
//        String dateAwal = "2017-02-14";
//        String dateAkhir = "2017-02-14";
        String kategori = "ID Penjualan";
        DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        String[] partsAwal = dateAwal.split("-");
        int part1Awal = Integer.parseInt(partsAwal[0]);
        int part2Awal = Integer.parseInt(partsAwal[1]);
        int part3Awal = Integer.parseInt(partsAwal[2]);

        String[] partsAkhir = dateAkhir.split("-");
        int part1Akhir = Integer.parseInt(partsAkhir[0]);
        int part2Akhir = Integer.parseInt(partsAkhir[1]);
        int part3Akhir = Integer.parseInt(partsAkhir[2]);

        System.out.println(part1Awal);
        System.out.println(part2Awal);
        System.out.println(part3Awal);
        System.out.println(part1Akhir);
        System.out.println(part2Akhir);
        System.out.println(part3Akhir);

        for (LocalDate ld = new LocalDate(part1Awal, part2Awal, part3Awal); ld.isBefore(new LocalDate(part1Akhir, part2Akhir, part3Akhir)) || ld.isEqual(new LocalDate(part1Akhir, part2Akhir, part3Akhir)); ld = ld.plusDays(1)) {
            // System.out.println(ld);
            listTanggal.add(ld);
        }

        String excelFileName = System.getProperty("user.home") + "/Desktop/Data Penjualan " + dateFormat.format(date) + ".xls";
        HSSFWorkbook wb = new HSSFWorkbook();

        for (LocalDate ld : listTanggal) {
            // System.out.println(ld);
            System.out.println("INIH: " + ld.toString());
            String sheetName = Function.returnFormattedDate(ld.toString());

            HSSFSheet sheet = wb.createSheet(sheetName);
            //untuk baris keberapa
            Row detailTitleRow = sheet.createRow(0);

            //untuk kolom keberapa
            Cell detailTitleCell = detailTitleRow.createCell(0);

            CellStyle detailTitleCellStyle = sheet.getWorkbook().createCellStyle();
            detailTitleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            detailTitleCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            detailTitleCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            detailTitleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            Font fontDetailTitle = sheet.getWorkbook().createFont();
            fontDetailTitle.setFontHeightInPoints((short) 14);
            fontDetailTitle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
            detailTitleCellStyle.setFont(fontDetailTitle);

            CellStyle detailTitleStyle = sheet.getWorkbook().createCellStyle();
            Font fontDetailTitleStyle = sheet.getWorkbook().createFont();
            fontDetailTitleStyle.setFontHeightInPoints((short) 20);
            fontDetailTitleStyle.setBoldweight(Font.BOLDWEIGHT_BOLD);
            detailTitleStyle.setFont(fontDetailTitleStyle);

            detailTitleCell.setCellStyle(detailTitleStyle);
//            detailTitleCell.setCellValue("Data Penjualan " + Function.returnFormattedDate(dateAwal) + " - " + Function.returnFormattedDate(dateAkhir));
            detailTitleCell.setCellValue("Data Penjualan " + sheetName);
            Row headerRow = sheet.createRow(2);
            Cell IDPenjualanHeaderCell = headerRow.createCell(0);
            Cell TglTransaksiHeaderCell = headerRow.createCell(1);
            Cell NamaBarangHeaderCell = headerRow.createCell(2);
            Cell HargaHeaderCell = headerRow.createCell(3);
            Cell BanyakBarangHeaderCell = headerRow.createCell(4);
            Cell TotalHargaHeaderCell = headerRow.createCell(5);
            //Cell TotalPenjualanHeaderCell = headerRow.createCell(5);

            IDPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);
            TglTransaksiHeaderCell.setCellStyle(detailTitleCellStyle);
            NamaBarangHeaderCell.setCellStyle(detailTitleCellStyle);
            HargaHeaderCell.setCellStyle(detailTitleCellStyle);
            BanyakBarangHeaderCell.setCellStyle(detailTitleCellStyle);
            TotalHargaHeaderCell.setCellStyle(detailTitleCellStyle);
            //TotalPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);

            IDPenjualanHeaderCell.setCellValue("ID Penjualan");
            TglTransaksiHeaderCell.setCellValue("Tanggal Transaksi");
            NamaBarangHeaderCell.setCellValue("Nama Barang");
            HargaHeaderCell.setCellValue("Harga");
            BanyakBarangHeaderCell.setCellValue("Banyak Barang");
            TotalHargaHeaderCell.setCellValue("Total Harga");

            String sql = "SELECT penjualan_detail.id_penjualan,penjualan_detail.id_detil_barang,penjualan_detail.harga,penjualan_detail.jumlah,barang_detail.nama_barang, penjualan.total_harga,penjualan_detail.tanggal_transaksi FROM `penjualan_detail` join penjualan on penjualan.id_penjualan=penjualan_detail.id_penjualan join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.tanggal_transaksi ='"+ld.toString()+"' ORDER by penjualan_detail.tanggal_transaksi DESC";
            String sqlTotal = "SELECT sum(total_harga) as Total FROM `penjualan` where tanggal_transaksi ='"+ld.toString()+"'";
            PreparedStatement ps;
            PreparedStatement psTotal;

            try {
                ps = kasirMain.Koneksi.getConnection().prepareStatement(sql);
                psTotal = kasirMain.Koneksi.getConnection().prepareStatement(sqlTotal);
                ResultSet resultSet = ps.executeQuery();
                ResultSet resultSetTotal = psTotal.executeQuery();
                int row = 3;

                while (resultSetTotal.next()) {
                    TotalHarga = resultSetTotal.getString("Total");
                }
                while (resultSet.next()) {
                    String IDPenjualan = resultSet.getString("id_penjualan");
                    String NamaBarang = resultSet.getString("nama_barang");
                    //String Harga = Function.addRp(Function.returnSeparator(resultSet.getString("harga")));
                    String Harga = resultSet.getString("harga");
                    String Jumlah = resultSet.getString("jumlah");
                    int hasil = Integer.parseInt(Harga) * Integer.parseInt(Jumlah);
                    String tglTransaksi = resultSet.getString("tanggal_transaksi");

                    Row dataRow = sheet.createRow(row);

                    Cell dataIDPenjualanCell = dataRow.createCell(0);
                    dataIDPenjualanCell.setCellValue(IDPenjualan);
                    dataIDPenjualanCell.setCellStyle(detailTitleCellStyle);

                    Cell dataTglTransaksiCell = dataRow.createCell(1);
                    dataTglTransaksiCell.setCellValue(tglTransaksi);
                    dataTglTransaksiCell.setCellStyle(detailTitleCellStyle);

                    Cell dataNamaBarangCell = dataRow.createCell(2);
                    dataNamaBarangCell.setCellValue(NamaBarang);
                    dataNamaBarangCell.setCellStyle(detailTitleCellStyle);

                    Cell dataHargaCell = dataRow.createCell(3);
                    dataHargaCell.setCellValue(Function.returnSeparator(Harga));
                    dataHargaCell.setCellStyle(detailTitleCellStyle);

                    Cell dataJumlahTransaksiCell = dataRow.createCell(4);
                    dataJumlahTransaksiCell.setCellValue(Jumlah);
                    dataJumlahTransaksiCell.setCellStyle(detailTitleCellStyle);

                    Cell dataHasilCell = dataRow.createCell(5);
                    dataHasilCell.setCellValue(Function.returnSeparator(String.valueOf(hasil)));
                    dataHasilCell.setCellStyle(detailTitleCellStyle);

                    row = row + 1;
                }
                Row dataRow = sheet.createRow(row);
                CellStyle style = wb.createCellStyle();
                style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
                style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
                Font font = wb.createFont();
                font.setColor(HSSFColor.RED.index);
                font.setFontHeightInPoints((short) 20);
                font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
                style.setFont(font);

                Cell CellTitleTotalHarga = dataRow.createCell(4);
                CellTitleTotalHarga.setCellValue("Total Harga");
                CellTitleTotalHarga.setCellStyle(detailTitleCellStyle);

                Cell dataTotalHargaCell = dataRow.createCell(5);
                dataTotalHargaCell.setCellValue(TotalHarga);
                dataTotalHargaCell.setCellStyle(style);

                Cell CellGenerated = dataRow.createCell(0);
                CellGenerated.setCellValue("Generated Time: " + Function.returnDateDayName(Function.getDateNow()) + " " + Function.getTimeNow());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
        }

        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(excelFileName);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            System.out.println("Excel Berhasil Dibuat");
            pesan = "Excel Berhasil Dibuat";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Excel Berhasil Dibuat");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Mohon Tutup Terlebih Dahulu Excel Anda");
            pesan = "Mohon Tutup Terlebih Dahulu Excel Anda";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Mohon Tutup Terlebih Dahulu Excel Anda");
        } catch (IOException ex) {
            //JOptionPane.showMessageDialog(this, "File Excel Gagal Dibuat");
            // Logger.getLogger(MenuDataAbsen.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("File Excel Gagal Dibuat");
            pesan = "File Excel Gagal Dibuat";
            return pesan;

        }

    }

    public static String createExcelByID(String idPenjualan) {
//        String idPenjualan = "310117000001";
        String kategori = "ID Penjualan";
        DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        String excelFileName = System.getProperty("user.home") + "/Desktop/Data Penjualan " + dateFormat.format(date) + ".xls";
        String sheetName = "Data Penjualan";
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet(sheetName);

        //untuk baris keberapa
        Row detailTitleRow = sheet.createRow(0);

        //untuk kolom keberapa
        Cell detailTitleCell = detailTitleRow.createCell(0);

        CellStyle detailTitleCellStyle = sheet.getWorkbook().createCellStyle();
        detailTitleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        Font fontDetailTitle = sheet.getWorkbook().createFont();
        fontDetailTitle.setFontHeightInPoints((short) 14);
        fontDetailTitle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
        detailTitleCellStyle.setFont(fontDetailTitle);

        CellStyle detailTitleStyle = sheet.getWorkbook().createCellStyle();
        Font fontDetailTitleStyle = sheet.getWorkbook().createFont();
        fontDetailTitleStyle.setFontHeightInPoints((short) 20);
        fontDetailTitleStyle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        detailTitleStyle.setFont(fontDetailTitleStyle);

        detailTitleCell.setCellStyle(detailTitleStyle);
        detailTitleCell.setCellValue("Data Penjualan ");

        Row headerRow = sheet.createRow(2);
        Cell IDPenjualanHeaderCell = headerRow.createCell(0);
        Cell NamaBarangHeaderCell = headerRow.createCell(1);
        Cell HargaHeaderCell = headerRow.createCell(2);
        Cell BanyakBarangHeaderCell = headerRow.createCell(3);
        Cell TotalHargaHeaderCell = headerRow.createCell(4);
        //Cell TotalPenjualanHeaderCell = headerRow.createCell(5);

        IDPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);
        NamaBarangHeaderCell.setCellStyle(detailTitleCellStyle);
        HargaHeaderCell.setCellStyle(detailTitleCellStyle);
        BanyakBarangHeaderCell.setCellStyle(detailTitleCellStyle);
        TotalHargaHeaderCell.setCellStyle(detailTitleCellStyle);
        //TotalPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);

        IDPenjualanHeaderCell.setCellValue("ID Penjualan");
        NamaBarangHeaderCell.setCellValue("Nama Barang");
        HargaHeaderCell.setCellValue("Harga");
        BanyakBarangHeaderCell.setCellValue("Banyak Barang");
        TotalHargaHeaderCell.setCellValue("Total Harga");
       // TotalPenjualanHeaderCell.setCellValue("Total Penjualan");

        //String sql = "SELECT * FROM `penjualan_detail` join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where id_penjualan like '"+idPenjualan+"' ORDER by tanggal_transaksi DESC";
        String sql = "SELECT penjualan_detail.id_penjualan,penjualan_detail.id_detil_barang,penjualan_detail.harga,penjualan_detail.jumlah,barang_detail.nama_barang, penjualan.total_harga FROM `penjualan_detail` join penjualan on penjualan.id_penjualan=penjualan_detail.id_penjualan join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.id_penjualan like '%" + idPenjualan + "%' ORDER by penjualan_detail.tanggal_transaksi DESC";
        String sqlTotal = "SELECT sum(harga) as Total FROM `penjualan_detail` where id_penjualan like '" + idPenjualan + "' ORDER by tanggal_transaksi DESC";
        PreparedStatement ps;

        try {
            ps = kasirMain.Koneksi.getConnection().prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            int row = 3;
            while (resultSet.next()) {
                String IDPenjualan = resultSet.getString("id_penjualan");
                String NamaBarang = resultSet.getString("nama_barang");
                //String Harga = Function.addRp(Function.returnSeparator(resultSet.getString("harga")));
                String Harga = resultSet.getString("harga");
                String Jumlah = resultSet.getString("jumlah");
                int hasil = Integer.parseInt(Harga) * Integer.parseInt(Jumlah);
                TotalHarga = resultSet.getString("total_harga");

                Row dataRow = sheet.createRow(row);

                Cell dataIDPenjualanCell = dataRow.createCell(0);
                dataIDPenjualanCell.setCellValue(IDPenjualan);
                dataIDPenjualanCell.setCellStyle(detailTitleCellStyle);

                Cell dataNamaBarangCell = dataRow.createCell(1);
                dataNamaBarangCell.setCellValue(NamaBarang);
                dataNamaBarangCell.setCellStyle(detailTitleCellStyle);

                Cell dataHargaCell = dataRow.createCell(2);
                dataHargaCell.setCellValue(Function.returnSeparator(Harga));
                dataHargaCell.setCellStyle(detailTitleCellStyle);

                Cell dataJumlahTransaksiCell = dataRow.createCell(3);
                dataJumlahTransaksiCell.setCellValue(Jumlah);
                dataJumlahTransaksiCell.setCellStyle(detailTitleCellStyle);

                Cell dataHasilCell = dataRow.createCell(4);
                dataHasilCell.setCellValue(Function.returnSeparator(String.valueOf(hasil)));
                dataHasilCell.setCellStyle(detailTitleCellStyle);

                row = row + 1;
            }
            Row dataRow = sheet.createRow(row);
            CellStyle style = wb.createCellStyle();
            style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            Font font = wb.createFont();
            font.setColor(HSSFColor.RED.index);
            font.setFontHeightInPoints((short) 20);
            font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
            style.setFont(font);

            Cell CellTitleTotalHarga = dataRow.createCell(3);
            CellTitleTotalHarga.setCellValue("Total Harga");
            CellTitleTotalHarga.setCellStyle(detailTitleCellStyle);

            Cell dataTotalHargaCell = dataRow.createCell(4);
            dataTotalHargaCell.setCellValue(Function.returnSeparator(TotalHarga));
            dataTotalHargaCell.setCellStyle(style);

            // Row dataRow1 = sheet.createRow(row);
            Cell CellGenerated = dataRow.createCell(0);
            CellGenerated.setCellValue("Generated Time: " + Function.returnDateDayName(Function.getDateNow()) + " " + Function.getTimeNow());
            //CellGenerated.setCellStyle(detailTitleCellStyle);

//            Cell dataDateCell = dataRow.createCell(1);
//            dataDateCell.setCellValue(Function.getDateNow()+" "+Function.getTimeNow());
//            dataDateCell.setCellStyle(style);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(excelFileName);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            System.out.println("Excel Berhasil Dibuat");
            pesan = "Excel Berhasil Dibuat";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Excel Berhasil Dibuat");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Mohon Tutup Terlebih Dahulu Excel Anda");
            pesan = "Mohon Tutup Terlebih Dahulu Excel Anda";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Mohon Tutup Terlebih Dahulu Excel Anda");
        } catch (IOException ex) {
            //JOptionPane.showMessageDialog(this, "File Excel Gagal Dibuat");
            // Logger.getLogger(MenuDataAbsen.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("File Excel Gagal Dibuat");
            pesan = "File Excel Gagal Dibuat";
            return pesan;

        }
    }

    public static String createExcelByDate(String dateAwal, String dateAkhir) {
        String idPenjualan = "310117000001";
//        String dateAwal = "2017-01-30";
//        String dateAkhir = "2017-02-08";
        String kategori = "ID Penjualan";
        DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        String excelFileName = System.getProperty("user.home") + "/Desktop/Data Penjualan " + dateFormat.format(date) + ".xls";
        String sheetName = "Data Penjualan";
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet(sheetName);

        //untuk baris keberapa
        Row detailTitleRow = sheet.createRow(0);

        //untuk kolom keberapa
        Cell detailTitleCell = detailTitleRow.createCell(0);

        CellStyle detailTitleCellStyle = sheet.getWorkbook().createCellStyle();
        detailTitleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        Font fontDetailTitle = sheet.getWorkbook().createFont();
        fontDetailTitle.setFontHeightInPoints((short) 14);
        fontDetailTitle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
        detailTitleCellStyle.setFont(fontDetailTitle);

        CellStyle detailTitleStyle = sheet.getWorkbook().createCellStyle();
        Font fontDetailTitleStyle = sheet.getWorkbook().createFont();
        fontDetailTitleStyle.setFontHeightInPoints((short) 20);
        fontDetailTitleStyle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        detailTitleStyle.setFont(fontDetailTitleStyle);

        detailTitleCell.setCellStyle(detailTitleStyle);
        detailTitleCell.setCellValue("Data Penjualan " + Function.returnFormattedDate(dateAwal) + " - " + Function.returnFormattedDate(dateAkhir));

        Row headerRow = sheet.createRow(2);
        Cell IDPenjualanHeaderCell = headerRow.createCell(0);
        Cell TglTransaksiHeaderCell = headerRow.createCell(1);
        Cell NamaBarangHeaderCell = headerRow.createCell(2);
        Cell HargaHeaderCell = headerRow.createCell(3);
        Cell BanyakBarangHeaderCell = headerRow.createCell(4);
        Cell TotalHargaHeaderCell = headerRow.createCell(5);
        //Cell TotalPenjualanHeaderCell = headerRow.createCell(5);

        IDPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);
        TglTransaksiHeaderCell.setCellStyle(detailTitleCellStyle);
        NamaBarangHeaderCell.setCellStyle(detailTitleCellStyle);
        HargaHeaderCell.setCellStyle(detailTitleCellStyle);
        BanyakBarangHeaderCell.setCellStyle(detailTitleCellStyle);
        TotalHargaHeaderCell.setCellStyle(detailTitleCellStyle);
        //TotalPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);

        IDPenjualanHeaderCell.setCellValue("ID Penjualan");
        TglTransaksiHeaderCell.setCellValue("Tanggal Transaksi");
        NamaBarangHeaderCell.setCellValue("Nama Barang");
        HargaHeaderCell.setCellValue("Harga");
        BanyakBarangHeaderCell.setCellValue("Banyak Barang");
        TotalHargaHeaderCell.setCellValue("Total Harga");
       // TotalPenjualanHeaderCell.setCellValue("Total Penjualan");

        //String sql = "SELECT * FROM `penjualan_detail` join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where id_penjualan like '"+idPenjualan+"' ORDER by tanggal_transaksi DESC";
        String sql = "SELECT penjualan_detail.id_penjualan,penjualan_detail.id_detil_barang,penjualan_detail.harga,penjualan_detail.jumlah,barang_detail.nama_barang, penjualan.total_harga,penjualan_detail.tanggal_transaksi FROM `penjualan_detail` join penjualan on penjualan.id_penjualan=penjualan_detail.id_penjualan join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.tanggal_transaksi BETWEEN '" + dateAwal + "' and '" + dateAkhir + "' ORDER by penjualan_detail.tanggal_transaksi DESC";
        String sqlTotal = "SELECT sum(total_harga) as Total FROM `penjualan` where tanggal_transaksi BETWEEN '" + dateAwal + "' and '" + dateAkhir + "'";
        PreparedStatement ps;
        PreparedStatement psTotal;

        try {
            ps = kasirMain.Koneksi.getConnection().prepareStatement(sql);
            psTotal = kasirMain.Koneksi.getConnection().prepareStatement(sqlTotal);
            ResultSet resultSet = ps.executeQuery();
            ResultSet resultSetTotal = psTotal.executeQuery();
            int row = 3;

            while (resultSetTotal.next()) {
                TotalHarga = resultSetTotal.getString("Total");
            }
            while (resultSet.next()) {
                String IDPenjualan = resultSet.getString("id_penjualan");
                String NamaBarang = resultSet.getString("nama_barang");
                //String Harga = Function.addRp(Function.returnSeparator(resultSet.getString("harga")));
                String Harga = resultSet.getString("harga");
                String Jumlah = resultSet.getString("jumlah");
                int hasil = Integer.parseInt(Harga) * Integer.parseInt(Jumlah);
                String tglTransaksi = resultSet.getString("tanggal_transaksi");

                Row dataRow = sheet.createRow(row);

                Cell dataIDPenjualanCell = dataRow.createCell(0);
                dataIDPenjualanCell.setCellValue(IDPenjualan);
                dataIDPenjualanCell.setCellStyle(detailTitleCellStyle);

                Cell dataTglTransaksiCell = dataRow.createCell(1);
                dataTglTransaksiCell.setCellValue(tglTransaksi);
                dataTglTransaksiCell.setCellStyle(detailTitleCellStyle);

                Cell dataNamaBarangCell = dataRow.createCell(2);
                dataNamaBarangCell.setCellValue(NamaBarang);
                dataNamaBarangCell.setCellStyle(detailTitleCellStyle);

                Cell dataHargaCell = dataRow.createCell(3);
                dataHargaCell.setCellValue(Function.returnSeparator(Harga));
                dataHargaCell.setCellStyle(detailTitleCellStyle);

                Cell dataJumlahTransaksiCell = dataRow.createCell(4);
                dataJumlahTransaksiCell.setCellValue(Jumlah);
                dataJumlahTransaksiCell.setCellStyle(detailTitleCellStyle);

                Cell dataHasilCell = dataRow.createCell(5);
                dataHasilCell.setCellValue(Function.returnSeparator(String.valueOf(hasil)));
                dataHasilCell.setCellStyle(detailTitleCellStyle);

                row = row + 1;
            }
            Row dataRow = sheet.createRow(row);
            CellStyle style = wb.createCellStyle();
            style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            Font font = wb.createFont();
            font.setColor(HSSFColor.RED.index);
            font.setFontHeightInPoints((short) 20);
            font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
            style.setFont(font);

            Cell CellTitleTotalHarga = dataRow.createCell(4);
            CellTitleTotalHarga.setCellValue("Total Harga");
            CellTitleTotalHarga.setCellStyle(detailTitleCellStyle);

            Cell dataTotalHargaCell = dataRow.createCell(5);
            dataTotalHargaCell.setCellValue(Function.returnSeparator(TotalHarga));
            dataTotalHargaCell.setCellStyle(style);

            Cell CellGenerated = dataRow.createCell(0);
            CellGenerated.setCellValue("Generated Time: " + Function.returnDateDayName(Function.getDateNow()) + " " + Function.getTimeNow());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(excelFileName);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            System.out.println("Excel Berhasil Dibuat");
            pesan = "Excel Berhasil Dibuat";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Excel Berhasil Dibuat");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Mohon Tutup Terlebih Dahulu Excel Anda");
            pesan = "Mohon Tutup Terlebih Dahulu Excel Anda";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Mohon Tutup Terlebih Dahulu Excel Anda");
        } catch (IOException ex) {
            //JOptionPane.showMessageDialog(this, "File Excel Gagal Dibuat");
            // Logger.getLogger(MenuDataAbsen.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("File Excel Gagal Dibuat");
            pesan = "File Excel Gagal Dibuat";
            return pesan;
        }
    }

    public static String createExcelByIdandDate(String idPenjualan, String dateAwal, String dateAkhir) {
//        String idPenjualan = "310117000001";
//        String dateAwal = "2017-01-30";
//        String dateAkhir = "2017-02-08";
        String kategori = "ID Penjualan";
        DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        String excelFileName = System.getProperty("user.home") + "/Desktop/Data Penjualan " + dateFormat.format(date) + ".xls";
        String sheetName = "Data Penjualan";
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet(sheetName);

        //untuk baris keberapa
        Row detailTitleRow = sheet.createRow(0);

        //untuk kolom keberapa
        Cell detailTitleCell = detailTitleRow.createCell(0);

        CellStyle detailTitleCellStyle = sheet.getWorkbook().createCellStyle();
        detailTitleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        detailTitleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        Font fontDetailTitle = sheet.getWorkbook().createFont();
        fontDetailTitle.setFontHeightInPoints((short) 14);
        fontDetailTitle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
        detailTitleCellStyle.setFont(fontDetailTitle);

        CellStyle detailTitleStyle = sheet.getWorkbook().createCellStyle();
        Font fontDetailTitleStyle = sheet.getWorkbook().createFont();
        fontDetailTitleStyle.setFontHeightInPoints((short) 20);
        fontDetailTitleStyle.setBoldweight(Font.BOLDWEIGHT_BOLD);
        detailTitleStyle.setFont(fontDetailTitleStyle);

        detailTitleCell.setCellStyle(detailTitleStyle);
        detailTitleCell.setCellValue("Data Penjualan " + dateAwal + " - " + dateAkhir);

        Row headerRow = sheet.createRow(2);
        Cell IDPenjualanHeaderCell = headerRow.createCell(0);
        Cell TglTransaksiHeaderCell = headerRow.createCell(1);
        Cell NamaBarangHeaderCell = headerRow.createCell(2);
        Cell HargaHeaderCell = headerRow.createCell(3);
        Cell BanyakBarangHeaderCell = headerRow.createCell(4);
        Cell TotalHargaHeaderCell = headerRow.createCell(5);
        //Cell TotalPenjualanHeaderCell = headerRow.createCell(5);

        IDPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);
        TglTransaksiHeaderCell.setCellStyle(detailTitleCellStyle);
        NamaBarangHeaderCell.setCellStyle(detailTitleCellStyle);
        HargaHeaderCell.setCellStyle(detailTitleCellStyle);
        BanyakBarangHeaderCell.setCellStyle(detailTitleCellStyle);
        TotalHargaHeaderCell.setCellStyle(detailTitleCellStyle);
        //TotalPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);

        IDPenjualanHeaderCell.setCellValue("ID Penjualan");
        TglTransaksiHeaderCell.setCellValue("Tanggal Transaksi");
        NamaBarangHeaderCell.setCellValue("Nama Barang");
        HargaHeaderCell.setCellValue("Harga");
        BanyakBarangHeaderCell.setCellValue("Banyak Barang");
        TotalHargaHeaderCell.setCellValue("Total Harga");
       // TotalPenjualanHeaderCell.setCellValue("Total Penjualan");

        //String sql = "SELECT * FROM `penjualan_detail` join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where id_penjualan like '"+idPenjualan+"' ORDER by tanggal_transaksi DESC";
        String sql = "SELECT penjualan_detail.id_penjualan,penjualan_detail.id_detil_barang,penjualan_detail.harga,penjualan_detail.jumlah,barang_detail.nama_barang, penjualan.total_harga,penjualan_detail.tanggal_transaksi FROM `penjualan_detail` join penjualan on penjualan.id_penjualan=penjualan_detail.id_penjualan join barang_detail on penjualan_detail.id_detil_barang=barang_detail.id_detil_barang where penjualan_detail.id_penjualan like '%" + idPenjualan + "%' and penjualan_detail.tanggal_transaksi BETWEEN '" + dateAwal + "' and '" + dateAkhir + "' ORDER by penjualan_detail.tanggal_transaksi DESC";
        String sqlTotal = "SELECT sum(total_harga) as Total FROM `penjualan` where id_penjualan like '%" + idPenjualan + "%' and tanggal_transaksi BETWEEN '" + dateAwal + "' and '" + dateAkhir + "'";
        System.out.println(sql);
        System.out.println(sqlTotal);
        PreparedStatement ps;
        PreparedStatement psTotal;

        try {
            ps = kasirMain.Koneksi.getConnection().prepareStatement(sql);
            psTotal = kasirMain.Koneksi.getConnection().prepareStatement(sqlTotal);
            ResultSet resultSet = ps.executeQuery();
            ResultSet resultSetTotal = psTotal.executeQuery();
            int row = 3;

            while (resultSetTotal.next()) {
                TotalHarga = resultSetTotal.getString("Total");
            }
            while (resultSet.next()) {
                String IDPenjualan = resultSet.getString("id_penjualan");
                String NamaBarang = resultSet.getString("nama_barang");
                //String Harga = Function.addRp(Function.returnSeparator(resultSet.getString("harga")));
                String Harga = resultSet.getString("harga");
                String Jumlah = resultSet.getString("jumlah");
                int hasil = Integer.parseInt(Harga) * Integer.parseInt(Jumlah);
                String tglTransaksi = resultSet.getString("tanggal_transaksi");

                Row dataRow = sheet.createRow(row);

                Cell dataIDPenjualanCell = dataRow.createCell(0);
                dataIDPenjualanCell.setCellValue(IDPenjualan);
                dataIDPenjualanCell.setCellStyle(detailTitleCellStyle);

                Cell dataTglTransaksiCell = dataRow.createCell(1);
                dataTglTransaksiCell.setCellValue(tglTransaksi);
                dataTglTransaksiCell.setCellStyle(detailTitleCellStyle);

                Cell dataNamaBarangCell = dataRow.createCell(2);
                dataNamaBarangCell.setCellValue(NamaBarang);
                dataNamaBarangCell.setCellStyle(detailTitleCellStyle);

                Cell dataHargaCell = dataRow.createCell(3);
                dataHargaCell.setCellValue(Function.returnSeparator(Harga));
                dataHargaCell.setCellStyle(detailTitleCellStyle);

                Cell dataJumlahTransaksiCell = dataRow.createCell(4);
                dataJumlahTransaksiCell.setCellValue(Jumlah);
                dataJumlahTransaksiCell.setCellStyle(detailTitleCellStyle);

                Cell dataHasilCell = dataRow.createCell(5);
                dataHasilCell.setCellValue(Function.returnSeparator(String.valueOf(hasil)));
                dataHasilCell.setCellStyle(detailTitleCellStyle);

                row = row + 1;
            }
            Row dataRow = sheet.createRow(row);
            CellStyle style = wb.createCellStyle();
            style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            Font font = wb.createFont();
            font.setColor(HSSFColor.RED.index);
            font.setFontHeightInPoints((short) 20);
            font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
            style.setFont(font);

            Cell CellTitleTotalHarga = dataRow.createCell(4);
            CellTitleTotalHarga.setCellValue("Total Harga");
            CellTitleTotalHarga.setCellStyle(detailTitleCellStyle);

            Cell dataTotalHargaCell = dataRow.createCell(5);
            dataTotalHargaCell.setCellValue(TotalHarga);
            dataTotalHargaCell.setCellStyle(style);

            Cell CellGenerated = dataRow.createCell(0);
            CellGenerated.setCellValue("Generated Time: " + Function.returnDateDayName(Function.getDateNow()) + " " + Function.getTimeNow());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(excelFileName);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            System.out.println("Excel Berhasil Dibuat");
            pesan = "Excel Berhasil Dibuat";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Excel Berhasil Dibuat");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Mohon Tutup Terlebih Dahulu Excel Anda");
            pesan = "Mohon Tutup Terlebih Dahulu Excel Anda";
            return pesan;
            // JOptionPane.showMessageDialog(this, "Mohon Tutup Terlebih Dahulu Excel Anda");
        } catch (IOException ex) {
            //JOptionPane.showMessageDialog(this, "File Excel Gagal Dibuat");
            // Logger.getLogger(MenuDataAbsen.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("File Excel Gagal Dibuat");
            pesan = "File Excel Gagal Dibuat";
            return pesan;
        }
    }

}
//    public static void createExcelSemua() {
//        String idPenjualan = "PEN-00002";
//        DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
//        Date date = new Date();
//        System.out.println(dateFormat.format(date));
//
//        String excelFileName = System.getProperty("user.home") + "/Desktop/Data Penjualan " + dateFormat.format(date) + ".xls";
//        String sheetName = "Data Penjualan";
//        HSSFWorkbook wb = new HSSFWorkbook();
//        HSSFSheet sheet = wb.createSheet(sheetName);
//
//        //untuk baris keberapa
//        Row detailTitleRow = sheet.createRow(0);
//
//        //untuk kolom keberapa
//        Cell detailTitleCell = detailTitleRow.createCell(0);
//
//        CellStyle detailTitleCellStyle = sheet.getWorkbook().createCellStyle();
//        detailTitleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
//        detailTitleCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
//        detailTitleCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
//        detailTitleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
//        Font fontDetailTitle = sheet.getWorkbook().createFont();
//        fontDetailTitle.setFontHeightInPoints((short) 14);
//        fontDetailTitle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
//        detailTitleCellStyle.setFont(fontDetailTitle);
//        
//        CellStyle detailTitleStyle = sheet.getWorkbook().createCellStyle();
//        Font fontDetailTitleStyle = sheet.getWorkbook().createFont();
//        fontDetailTitleStyle.setFontHeightInPoints((short) 20);
//        fontDetailTitleStyle.setBoldweight(Font.BOLDWEIGHT_NORMAL);
//        detailTitleStyle.setFont(fontDetailTitleStyle);
//
//        detailTitleCell.setCellStyle(detailTitleStyle);
//        detailTitleCell.setCellValue("Data Penjualan " + dateFormat.format(date));
//
//        Row headerRow = sheet.createRow(2);
//        Cell IDPenjualanHeaderCell = headerRow.createCell(0);
//        Cell TotalTerjualHeaderCell = headerRow.createCell(1);
//        Cell TotalHargaHeaderCell = headerRow.createCell(2);
//        Cell TanggalTransaksiHeaderCell = headerRow.createCell(3);
//        Cell WaktuTransaksiHeaderCell = headerRow.createCell(4);
//
//        IDPenjualanHeaderCell.setCellStyle(detailTitleCellStyle);
//        TotalTerjualHeaderCell.setCellStyle(detailTitleCellStyle);
//        TotalHargaHeaderCell.setCellStyle(detailTitleCellStyle);
//        TanggalTransaksiHeaderCell.setCellStyle(detailTitleCellStyle);
//        WaktuTransaksiHeaderCell.setCellStyle(detailTitleCellStyle);
//
//        IDPenjualanHeaderCell.setCellValue("ID Penjualan");
//        TotalTerjualHeaderCell.setCellValue("Total Barang Terjual");
//        TotalHargaHeaderCell.setCellValue("Total Harga");
//        TanggalTransaksiHeaderCell.setCellValue("Tanggal Transaksi");
//        WaktuTransaksiHeaderCell.setCellValue("Waktu Transaksi");
//
//        String sql = "SELECT * FROM `penjualan` where id_penjualan like '%" + idPenjualan + "%' ORDER by tanggal_transaksi DESC";
//        PreparedStatement ps;
//
//        try {
//            ps = kasirMain.Koneksi.getConnection().prepareStatement(sql);
//            ResultSet resultSet = ps.executeQuery();
//            int row = 3;
//            while (resultSet.next()) {
//                String IDPenjualan = resultSet.getString("id_penjualan");
//                String TotalTerjual = resultSet.getString("total_barang");
//                String TotalHarga = Function.addRp(Function.returnSeparator(resultSet.getString("total_harga")));
//                String TanggalTransaksi = resultSet.getString("tanggal_transaksi");
//                String WaktuTransaksi = resultSet.getString("waktu_transaksi");
//
//                Row dataRow = sheet.createRow(row);
//                Cell dataIDPenjualanCell = dataRow.createCell(0);
//                dataIDPenjualanCell.setCellValue(IDPenjualan);
//                dataIDPenjualanCell.setCellStyle(detailTitleCellStyle);
//
//                Cell dataTotalTerjualCell = dataRow.createCell(1);
//                dataTotalTerjualCell.setCellValue(TotalTerjual);
//                dataTotalTerjualCell.setCellStyle(detailTitleCellStyle);
//
//                Cell dataTotalHargaCell = dataRow.createCell(2);
//                dataTotalHargaCell.setCellValue(TotalHarga);
//                dataTotalHargaCell.setCellStyle(detailTitleCellStyle);
//
//                Cell dataTanggalTransaksiCell = dataRow.createCell(3);
//                dataTanggalTransaksiCell.setCellValue(TanggalTransaksi);
//                dataTanggalTransaksiCell.setCellStyle(detailTitleCellStyle);
//
//                Cell dataWaktuTransaksiCell = dataRow.createCell(4);
//                dataWaktuTransaksiCell.setCellValue(WaktuTransaksi);
//                dataWaktuTransaksiCell.setCellStyle(detailTitleCellStyle);
//
//                row = row + 1;
//
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        sheet.autoSizeColumn(0);
//        sheet.autoSizeColumn(1);
//        sheet.autoSizeColumn(2);
//        sheet.autoSizeColumn(3);
//        sheet.autoSizeColumn(4);
//        FileOutputStream fileOut;
//        try {
//            fileOut = new FileOutputStream(excelFileName);
//            wb.write(fileOut);
//            fileOut.flush();
//            fileOut.close();
//            System.out.println("Excel Berhasil Dibuat");
//            // JOptionPane.showMessageDialog(this, "Excel Berhasil Dibuat");
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//            System.out.println("Mohon Tutup Terlebih Dahulu Excel Anda");
//            // JOptionPane.showMessageDialog(this, "Mohon Tutup Terlebih Dahulu Excel Anda");
//        } catch (IOException ex) {
//            //JOptionPane.showMessageDialog(this, "File Excel Gagal Dibuat");
//            // Logger.getLogger(MenuDataAbsen.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//            System.out.println("File Excel Gagal Dibuat");
//        }
//    }

//}
