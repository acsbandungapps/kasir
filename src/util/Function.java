/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Rectangle;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JTable;

/**
 *
 * @author Erdy
 */
public class Function {

    public static String convertFloat(Double nilai) {
        DecimalFormat df = new DecimalFormat("####0.00");
        System.out.println("Value: " + df.format(nilai));
        //return Double.valueOf(String.format("%.2f", nilai));
        return df.format(nilai);
    }

    public static String removeRp(String Rp) {
        String s = Rp.replace("Rp. ", "");
        return s;
    }

    public static String addRp(String value) {
        String s = "Rp " + value;
        return s;
    }

    public static void generateIDBarangDetail() {

    }

    public static void scrollToRect(JTable table, int nextSelectedRow) {
        Rectangle currentVisible = table.getVisibleRect();
        Rectangle scrollToRect = table.getCellRect(nextSelectedRow, 0, true);
        if (scrollToRect.getY() > currentVisible.getY() + currentVisible.getHeight()) {
            scrollToRect.setLocation(0,
                    (int) (scrollToRect.getY() + currentVisible.getHeight() - scrollToRect.getHeight()));
        }
        table.scrollRectToVisible(scrollToRect);
    }

    public static String getDateNow() {
        DateFormat datef = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String tgl = datef.format(date);
        return tgl;
    }

    public static String getTimeNow() {
        DateFormat datef = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String tgl = datef.format(date);
        return tgl;
    }

    public static String returnSeparator(String n) {
        BigDecimal bd = new BigDecimal(n);
        NumberFormat formatter = NumberFormat.getInstance(new Locale("en_US"));
        return formatter.format(bd.longValue());
    }
    
     public static String returnIntSeparator(int n) {
        BigDecimal bd = new BigDecimal(n);
        NumberFormat formatter = NumberFormat.getInstance(new Locale("en_US"));
        return formatter.format(bd.longValue());
    }
     
     

    public static String removeSeparator(String n) {
        String s = n.replace(",", "");
        return s;
    }
    
    public static String returnFormattedDate(String date){
        DateFormat dftgl = new SimpleDateFormat("yyyy-mm-dd");
        DateFormat dftgl1 = new SimpleDateFormat("dd-mm-yyyy");

        String tgrawat = "2017-11-31";
        Date waktu1 = null;

        try {
            waktu1 = dftgl.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String time = dftgl1.format(waktu1);
        System.out.println("Hasil: " + time);
        return time;
    }
    
    public static String returnDateDayName(String date){
        final String NEW_FORMAT = "EEEE,dd-MMM-yyyy";
        final String OLD_FORMAT = "yyyy-MM-dd";
        
        String oldDateString = "2011-11-17";
        String newDateString="";
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date dd = sdf.parse(date);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(dd);
            System.out.println(newDateString);
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
        return newDateString;
    }
}
