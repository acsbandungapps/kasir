/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Satuan_Barang;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Erdy
 */
public class Model_Satuan_Barang extends AbstractTableModel{
     private List<Satuan_Barang> listSatuan;

    public Model_Satuan_Barang(List<Satuan_Barang> list) {
        listSatuan = list;
    }

    @Override
    public int getRowCount() {
        return listSatuan.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID Satuan";
            case 1:
                return "Nama Satuan";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Satuan_Barang s = listSatuan.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return s.getId_satuan();
            case 1:
                return s.getNama_satuan();

            default:
                return "";
        }
    }

    public Satuan_Barang getSatuan(int rowIndex) {
        return listSatuan.get(rowIndex);
    }

    public List<Satuan_Barang> getAllDataSatuanBarang() {
        return listSatuan;
    }
}
