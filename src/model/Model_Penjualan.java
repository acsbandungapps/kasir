/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Keranjang;
import entity.Penjualan;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Erdy
 */
public class Model_Penjualan extends AbstractTableModel {

    private List<Penjualan> listPenjualan;

    public Model_Penjualan(List<Penjualan> list) {
        listPenjualan = list;
    }

    @Override
    public int getRowCount() {
        return listPenjualan.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID Penjualan";
            case 1:
                return "Total Barang Terjual";
            case 2:
                return "Total Harga";
            case 3:
                return "Uang Dibayar";
            case 4:
                return "Tanggal Transaksi";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Penjualan p = listPenjualan.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return p.getId_penjualan();
            case 1:
                return p.getTotal_barang();
            case 2:
                return p.getTotal_harga();
            case 3:
                return p.getUang_dibayar();
            case 4:
                return p.getTanggal_transaksi();
            default:
                return "";
        }
    }

    public Penjualan getPenjualan(int rowIndex) {
        return listPenjualan.get(rowIndex);
    }

    public List<Penjualan> getAllData() {
        return listPenjualan;
    }
}
