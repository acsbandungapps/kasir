/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Barang_Detail;
import entity.Keranjang;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Erdy
 */
public class Model_Keranjang extends AbstractTableModel {

    private List<Keranjang> listKeranjang;

    public Model_Keranjang(List<Keranjang> list) {
        listKeranjang = list;
    }

    @Override
    public int getRowCount() {
        return listKeranjang.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nama Kategori";
            case 1:
                return "Nama Barang";
            case 2:
                return "Harga Jual";
            case 3:
                return "Banyak Beli";
            case 4:
                return "Total Harga";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Keranjang k = listKeranjang.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return k.getNamaKategoriBarang();
            case 1:
                return k.getNamaBarang();
            case 2:
                return k.getHargaJual();
            case 3:
                return k.getJumlah_beli();
            case 4:
                return k.getTotal();
            default:
                return "";
        }
    }

    public Keranjang getKeranjang(int rowIndex) {
        return listKeranjang.get(rowIndex);
    }

    public List<Keranjang> getAllData() {
        return listKeranjang;
    }
}
