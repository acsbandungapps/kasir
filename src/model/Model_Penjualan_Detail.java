/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Penjualan_Detail;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import util.Function;

/**
 *
 * @author Erdy
 */
public class Model_Penjualan_Detail extends AbstractTableModel {

    private List<Penjualan_Detail> listPenjualanDetail;

    public Model_Penjualan_Detail(List<Penjualan_Detail> list) {
        listPenjualanDetail = list;
    }

    @Override
    public int getRowCount() {
        return listPenjualanDetail.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID Penjualan";
            case 1:
                return "ID Barang";
            case 2:
                return "Nama Barang";
            case 3:
                return "Harga";
            case 4:
                return "Jumlah Pembelian";
            case 5:
                return "Total Harga";
            case 6:
                return "Tanggal Transaksi";
            case 7:
                return "Waktu Transaksi";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Penjualan_Detail p = listPenjualanDetail.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return p.getId_penjualan();
            case 1:
                return p.getId_detil_barang();
            case 2:
                return p.getNamaBarang();
            case 3:
                return p.getHarga();
            case 4:
                return p.getJumlah();
            case 5:
                //return Integer.parseInt(Function.removeRp(Function.removeSeparator(p.getHarga())))*Integer.parseInt(Function.removeSeparator(Function.removeRp(String.valueOf(p.getJumlah()))));
            return p.getTotal();
            case 6:
                return p.getTanggal_transaksi();
            case 7:
                return p.getWaktu_transaksi();
            default:
                return "";
        }
    }

    public Penjualan_Detail getPenjualanDetail(int rowIndex) {
        return listPenjualanDetail.get(rowIndex);
    }

    public List<Penjualan_Detail> getAllData() {
        return listPenjualanDetail;
    }
}
