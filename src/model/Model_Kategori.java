/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Kategori;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Erdy
 */
public class Model_Kategori extends AbstractTableModel {

    private List<Kategori> listKategori;

    public Model_Kategori(List<Kategori> list) {
        listKategori = list;
    }

    @Override
    public int getRowCount() {
        return listKategori.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID Kategori";
            case 1:
                return "Nama Kategori";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Kategori k = listKategori.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return k.getIdKategori();
            case 1:
                return k.getNamaKategori();
            default:
                return "";
        }
    }

    public Kategori getKategori(int rowIndex) {
        return listKategori.get(rowIndex);
    }

    public List<Kategori> getAllDataKategori() {
        return listKategori;
    }
}
