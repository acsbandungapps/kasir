/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Barang_Detail;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Erdy
 */
public class Model_Cari_Barang extends AbstractTableModel{
    private List<Barang_Detail> listBarang;

    public Model_Cari_Barang(List<Barang_Detail> list) {
        listBarang = list;
    }

    @Override
    public int getRowCount() {
        return listBarang.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID Barang";
            case 1:
                return "Kategori";
            case 2:
                return "Nama Barang";
            case 3:
                return "Jumlah Pembelian";
            case 4:
                return "Jumlah Stok";
            case 5:
                return "Harga Beli";
            case 6:
                return "Harga Jual";
            case 7:
                return "Laba";
            case 8:
                return "Satuan";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Barang_Detail b = listBarang.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return b.getIdDetilBarang();
            case 1:
                return b.getNamaKategori();
            case 2:
                return b.getNamaBarang();
            case 3:
                return b.getJumlahPembelian();
            case 4:
                return b.getJumlahStok();
            case 5:
                return b.getHargaBeli();
            case 6:
                return b.getHargaJual();
            case 7:
                return b.getLaba();
            case 8:
                return b.getNamaSatuan();
            default:
                return "";
        }
    }

    public Barang_Detail getBarang(int rowIndex) {
        return listBarang.get(rowIndex);
    }

    public List<Barang_Detail> getAllData() {
        return listBarang;
    }
}
