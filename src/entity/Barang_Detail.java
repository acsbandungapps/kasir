/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Erdy
 */
public class Barang_Detail {

//    public String id_detail_barang;
//    public String id_barang;
//    public String nama_barang;
//    public String harga_barang;
//    public int jumlah_pembelian;
//    public String nama_kategori_barang;
//    public String id_kategori;
//    public String namaKategori;
//    public String id_stok;
//    public String jumlah_stok;
//    public String harga_jual;
//    public String id_satuan;
//    public String nama_satuan;
//    public String harga_beli;
    public String idDetilBarang;
    public String idKategori;
    public String namaKategori;
    public String namaBarang;
    public int jumlahPembelian;
    public int jumlahStok;
    public String hargaBeli;
    public String hargaJual;
    public String laba;
    public String idSatuan;
    public String namaSatuan;
    public String status;

    public String getIdDetilBarang() {
        return idDetilBarang;
    }

    public void setIdDetilBarang(String idDetilBarang) {
        this.idDetilBarang = idDetilBarang;
    }

    public String getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(String idKategori) {
        this.idKategori = idKategori;
    }

    public String getNamaKategori() {
        return namaKategori;
    }

    public void setNamaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public int getJumlahPembelian() {
        return jumlahPembelian;
    }

    public void setJumlahPembelian(int jumlahPembelian) {
        this.jumlahPembelian = jumlahPembelian;
    }

    public int getJumlahStok() {
        return jumlahStok;
    }

    public void setJumlahStok(int jumlahStok) {
        this.jumlahStok = jumlahStok;
    }

    public String getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(String hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public String getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(String hargaJual) {
        this.hargaJual = hargaJual;
    }

    public String getLaba() {
        return laba;
    }

    public void setLaba(String laba) {
        this.laba = laba;
    }

    public String getIdSatuan() {
        return idSatuan;
    }

    public void setIdSatuan(String idSatuan) {
        this.idSatuan = idSatuan;
    }

    public String getNamaSatuan() {
        return namaSatuan;
    }

    public void setNamaSatuan(String namaSatuan) {
        this.namaSatuan = namaSatuan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
}
