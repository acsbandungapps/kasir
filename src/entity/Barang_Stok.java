/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Erdy
 */
public class Barang_Stok {
    public String id_stok_barang;
    public String id_barang;
    public String id_detil_barang;
    public String nama_barang;
    public int jumlah_stok;
    public Double harga_beli;
    public Double harga_jual;
    public String id_satuan;

    public String getId_stok_barang() {
        return id_stok_barang;
    }

    public void setId_stok_barang(String id_stok_barang) {
        this.id_stok_barang = id_stok_barang;
    }

    public String getId_barang() {
        return id_barang;
    }

    public void setId_barang(String id_barang) {
        this.id_barang = id_barang;
    }

    public String getId_detil_barang() {
        return id_detil_barang;
    }

    public void setId_detil_barang(String id_detil_barang) {
        this.id_detil_barang = id_detil_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public int getJumlah_stok() {
        return jumlah_stok;
    }

    public void setJumlah_stok(int jumlah_stok) {
        this.jumlah_stok = jumlah_stok;
    }

    public Double getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(Double harga_beli) {
        this.harga_beli = harga_beli;
    }

    public Double getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(Double harga_jual) {
        this.harga_jual = harga_jual;
    }

    public String getId_satuan() {
        return id_satuan;
    }

    public void setId_satuan(String id_satuan) {
        this.id_satuan = id_satuan;
    }
    
    
    
    
    
}
