/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Erdy
 */
public class Penjualan_Detail {

    private String id_detil_penjualan;
    private String id_penjualan;
    private String id_barang;
    private String id_detil_barang;
    private String tanggal_transaksi;
    private Time waktu_transaksi;
    private String harga;
    private int jumlah;
    private String total;
    private String status;
    private String namaBarang;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    
    public String getId_detil_penjualan() {
        return id_detil_penjualan;
    }

    public void setId_detil_penjualan(String id_detil_penjualan) {
        this.id_detil_penjualan = id_detil_penjualan;
    }

    public String getId_penjualan() {
        return id_penjualan;
    }

    public void setId_penjualan(String id_penjualan) {
        this.id_penjualan = id_penjualan;
    }

    public String getId_barang() {
        return id_barang;
    }

    public void setId_barang(String id_barang) {
        this.id_barang = id_barang;
    }

    public String getId_detil_barang() {
        return id_detil_barang;
    }

    public void setId_detil_barang(String id_detil_barang) {
        this.id_detil_barang = id_detil_barang;
    }

    public String getTanggal_transaksi() {
        return tanggal_transaksi;
    }

    public void setTanggal_transaksi(String tanggal_transaksi) {
        this.tanggal_transaksi = tanggal_transaksi;
    }

  

    public Time getWaktu_transaksi() {
        return waktu_transaksi;
    }

    public void setWaktu_transaksi(Time waktu_transaksi) {
        this.waktu_transaksi = waktu_transaksi;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

   
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

}
