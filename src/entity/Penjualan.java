/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Erdy
 */
public class Penjualan {

    private String id_penjualan;
    private int total_barang;
    private String total_harga;
    private String uang_dibayar;
    private String kembalian;
    private String status;
    private String tanggal_transaksi;
    private Time waktu_transaksi;

    public String getId_penjualan() {
        return id_penjualan;
    }

    public void setId_penjualan(String id_penjualan) {
        this.id_penjualan = id_penjualan;
    }

    public int getTotal_barang() {
        return total_barang;
    }

    public void setTotal_barang(int total_barang) {
        this.total_barang = total_barang;
    }

    public String getTotal_harga() {
        return total_harga;
    }

    public void setTotal_harga(String total_harga) {
        this.total_harga = total_harga;
    }

    public String getUang_dibayar() {
        return uang_dibayar;
    }

    public void setUang_dibayar(String uang_dibayar) {
        this.uang_dibayar = uang_dibayar;
    }

    public String getKembalian() {
        return kembalian;
    }

    public void setKembalian(String kembalian) {
        this.kembalian = kembalian;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal_transaksi() {
        return tanggal_transaksi;
    }

    public void setTanggal_transaksi(String tanggal_transaksi) {
        this.tanggal_transaksi = tanggal_transaksi;
    }

  

    public Time getWaktu_transaksi() {
        return waktu_transaksi;
    }

    public void setWaktu_transaksi(Time waktu_transaksi) {
        this.waktu_transaksi = waktu_transaksi;
    }

    
    
}
