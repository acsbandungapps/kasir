/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Erdy
 */
public class Satuan_Barang {
    public String id_satuan;
    public String nama_satuan;

    public String getId_satuan() {
        return id_satuan;
    }

    public void setId_satuan(String id_satuan) {
        this.id_satuan = id_satuan;
    }

    public String getNama_satuan() {
        return nama_satuan;
    }

    public void setNama_satuan(String nama_satuan) {
        this.nama_satuan = nama_satuan;
    }

    @Override
    public String toString() {
        return getNama_satuan(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
